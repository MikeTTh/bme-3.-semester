# bmeddit
*Egy reddit által instpirált közösségi háló.*

## Posztok
Az oldal lényege, hogy meg lehet osztani szövegeket és képeket.  
Ebből adódóan egy poszt tárolása a következőképpen néz ki:
```json
{
  "ID": "dfsdfsi56uh849yt",
  "Title": "Szerver oldali JavaScript",
  "Text": "Tök jó szabvált találtam, srácok!"
}
``` 
Egy képet tartalmazó post pedig így épül fel:
```json
{
  "ID": "jasijshdf87sdfds",
  "Title": "cuki cica",
  "Text": "nédd má milyen cuki",
  "Image": "kepidsokszamw5345uretr.png"  // mentett képre hivatkozás
}
```
A posztokat lehet szerkeszteni és törölni, a létrehozással azonos felületről.

(Amennyiben a MongoDB-ben máshogy idiomatikus a posztokra hivatkozni, az esetben az ID mezőt megfelelően alakítom)

## Felhasználók
A felhasználókat a következőképpen tárolom:
```json
{
  "Username": "Béla",          // megjelenített név
  "Posts": [                   // user posztjainak id-i
    "dfsdfsi56uh849yt",
    "jasijshdf87sdfds"
  ],
  "Email": "asd@asd.com",      // user email címe
  "PwHash": "fshdfiojsdiofj"   // hashelt jelszó
}
```

## Oldalak

### Kezdőoldal
A kezdőoldalon jelennek meg a legfrisseb posztok egymás alatt.  
![Főoldal](Posts.png)  
Ez egy végtelenül görgethető lista, idő szerint rendezve.

### Bejelentkezés
Az oldalak tetején lévő bar-ból lehet bejelentkezni vagy regisztrálni:  
![Bejelentkezés](Login.png)

### Posztok létrehozása
A főoldalon a ceruza ikonra kattintva egy olyan oldalra jutunk, ahol tudunk
posztokat létrehozni és beküldeni. Itt tudunk képet is felcsatolni.  
![Posztolás](Create.png)

---
title: bmeddit
author: Tóth Miklós (FLGIIG)
geometry: margin=2cm
output: pdf_document
---