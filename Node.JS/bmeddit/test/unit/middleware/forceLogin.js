const expect = require('chai').expect;
const forceLogin = require('../../../src/middleware/forceLogin');

describe("forceLogin middleware", function() {
    const mw = forceLogin({});
    it('should call next if user exists in session', function (done) {
        const req = {
            session: {
                user: {
                    name: "Józsi"
                }
            }
        };

        mw(req, {}, function (err) {
            expect(err).to.eql(undefined);
            done();
        });
    });

    it("should not call next if user doesn't exist in session" +
        "and it should redirect to /login", function (done) {
        const req = {
            session: {}
        };
        const res = {
            redirect: function (path) {
                expect(path).to.eql("/login");
                done();
            }
        };
        const next = function () {
            expect.fail("next should not be called");
        };
        mw(req, res, next);
    });
});