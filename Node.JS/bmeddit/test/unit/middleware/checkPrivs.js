const expect = require('chai').expect;
const checkPrivs = require('../../../src/middleware/checkPrivs');

describe("checkPrivs middleware", function() {
    const validUserId = "asdasdasd";
    const testedPostId = "postasdasdasd";

    const validSession = {
        user: {
            _id: validUserId,
        }
    };
    
    const validParams = {
        id: testedPostId,
    };
    
    const PostModel = {
        findOne: function (obj, cb) {
            expect(obj).to.have.key("_id");
            if (obj._id === testedPostId) {
                cb(null, {
                    user: validUserId,
                });
            } else {
                cb("lol, error", {});
            }
        },
    };
    
    const objRepo = { PostModel };
    
    const mw = checkPrivs(objRepo);

    
    const resRedirToRoot = function (done) {
        return {
            redirect: function (path) {
                expect(path).to.eql("/");
                done();
            },
        };
    };
    
    it('should redirect to / if req.params.id is missing', function (done) {
        const req = {
            params: {},
            session: validSession,
        };
        mw(req, resRedirToRoot(done), objRepo);
    });

    it('should redirect to / if session.user is not valid', function (done) {
        const req = {
            params: validParams,
            session: {},
        };
        mw(req, resRedirToRoot(done), objRepo);
    });

    it('should call next if post.user equals session.user._id and res.locals.post to be set', function (done) {
        const req = {
            params: validParams,
            session: validSession,
        }
        
        const res = {
            locals: {},
        };

        mw(req, res, function (err){
            expect(err).to.eql(undefined);
            
            expect(res.locals.post.user).to.eql(validUserId);
            
            done();
        });
    });

    it("should redirect to / if post.user doesn't equal session.user._id and not call next", function (done) {
        const req = {
            params: validParams,
            session: validSession,
        };
        req.session.user._id = "i'm an invalid id";

        mw(req, resRedirToRoot(done), function (){
            expect.fail("next should not be called");
        });
    });

    it('should redirect to / if post is not found', function (done) {
        const req = {
            params: validParams,
            session: validSession,
        };

        req.params.id = "invalid";

        mw(req, resRedirToRoot(done), function (){
            expect.fail("next should not be called");
        });
    });
});