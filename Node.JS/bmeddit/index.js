const express = require("express");
const routes = require("./src/routes");
const session = require('express-session');
const FileStore = require("session-file-store")(session);
const multer = require("multer");
const fs = require('fs');

const app = express();

app.set('view engine', 'ejs');
app.use(express.urlencoded({
    extended: true,
    limit: "20MB"
}));
app.use(express.json());

const dir = 'static/uploads';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}

app.use(multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, dir)
        },
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now())
        }
    }),
}).single("image"));


app.use(
    session({
        secret: '🐘💨',
        resave: false,
        saveUninitialized: true,
        store: new FileStore({})
    })
);

app.use(express.static("static"));

routes(app);

app.listen(8080, () => {
    console.log("Listening on :8080");
});