const express = require("express");

const render = require("./middleware/render");
const forceLogin = require("./middleware/forceLogin");
const checkPrivs = require("./middleware/checkPrivs");
const login = require("./middleware/login");
const redirect = require("./middleware/redirect");
const register = require("./middleware/register");
const deletePost = require("./middleware/deletePost");
const savePost = require("./middleware/savePost");
const getPost = require("./middleware/getPost");
const getPosts = require("./middleware/getPosts");
const logout = require("./middleware/logout")

const UserModel = require("./db/User")
const PostModel = require("./db/Post")

module.exports = function (app) {
    const objRepo = {
        UserModel,
        PostModel
    };


    app.use(
        "/post/new",
        forceLogin(objRepo),
        savePost(objRepo),
        render(objRepo, "edit")
    );

    app.use(
        "/post/edit/:id",
        forceLogin(objRepo),
        checkPrivs(objRepo),
        getPost(objRepo),
        savePost(objRepo),
        render(objRepo, "edit")
    );

    app.use(
        "/post/del/:id",
        forceLogin(objRepo),
        checkPrivs(objRepo),
        deletePost(objRepo),
        redirect("/")
    );


    app.use(
        "/login",
        login(objRepo),
        render(objRepo, "login")
    );

    app.use(
        "/register",
        register(objRepo),
        render(objRepo, "login")
    );

    app.use(
        "/logout",
        logout(objRepo),
        redirect("/")
    );

    app.use(
        "/",
        getPosts(objRepo),
        render(objRepo, "index")
    );

}