module.exports = function require(obj, ...keys) {
    for (let i = 0; i < keys.length; i++) {
        if (typeof obj[keys[i]] === "undefined") {
            return false;
        }
        if (!obj[keys[i]]) {
            return false;
        }
    }
    return true;
}