const needs = require("../needs");
const bcrypt = require("bcrypt");

/**
 * Bejelentkezteti a felhasználót, sütik, meg minden... 🍪
 */
module.exports = function (objRepo) {
    const UserModel = objRepo.UserModel;
    return function (req, res, next) {
        if(req.method !== "POST") {
            return next();
        }
        if (needs(req.body, "email")) {
            res.locals.email = req.body.email;
        }

        if (!needs(req.body, "email", "password")) {
            res.locals.error = "Ki kell tölteni miden mezőt"
            return next();
        }

        UserModel.findOne({email: req.body.email}, (err, user) => {
            if (err) {
                return next(err);
            }
            
            if (user == null) {
                res.locals.error = "Nincs ilyen felhasználó";
                return next();
            }

            bcrypt.compare(req.body.password, user.pwHash, (err, match) => {
                if (err)
                    return next(err);
                if (match) {
                    req.session.user = user;
                    req.session.save(err => {
                        if (err) {
                            next(err);
                        }
                        return res.redirect('/');
                    });
                } else {
                    res.locals.error = 'Rossz jelszó';
                    return next();
                }
            });
        });
    };
}