module.exports = function (path) {
    return function (req, res, next) {
        delete req.session.user;
        return next();
    };
}