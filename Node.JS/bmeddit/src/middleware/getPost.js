/**
 * Betölti egy adott id-jű post adatait
 */
module.exports = function (objRepo) {
    const Post = objRepo.PostModel;

    return function (req, res, next) {
        if (typeof res.locals.post !== "undefined" && res.locals.post) {
            return next();
        }
        Post.findOne({_id: req.params.id}, (err, post) => {
            res.locals.post = post;
            return next();
        });
    };
}