/**
 * Betölti az összes postot
 */
module.exports = function (objRepo) {
    return function (req, res, next) {
        res.locals.posts = [];

        objRepo.PostModel.find({}, null, { sort: { date: "desc" } }, (err, list) => {
            if (err)
                return next(err);
            res.locals.posts = list;
            
            for (let i = 0; i < res.locals.posts.length; i++) {
                if (typeof req.session.user !== "undefined" && res.locals.posts[i].user+"" === req.session.user._id) {
                    res.locals.posts[i].canedit = true;
                }
            }
            
            return next();
        });
    };
}