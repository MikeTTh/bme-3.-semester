const needs = require('../needs');
/**
 * Ellenőrzi, hogy a usernek van-e jogosultsága
 */
module.exports = function (objRepo) {
    const Post = objRepo.PostModel;
    return function (req, res, next) {
        if (!needs(req.params, "id") || !needs(req.session, "user")){
            return res.redirect('/'); // TODO: no permissions site
        }
        
        Post.findOne({_id: req.params.id}, (err, post) => {
            if (err) {
                return res.redirect('/'); // TODO: no permissions site
            }
            if (req.session.user._id === post.user+"") {
                res.locals.post = post;
                return next();
            }
            return res.redirect('/'); // TODO: no permissions site
        });
    };
}