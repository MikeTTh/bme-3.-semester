/**
 * Ellenőrzi, ha nincs belépve felhasználó, akkor átdobjuk /login-ba.
 */
module.exports = function (objRepo) {
    return function (req, res, next) {
        if (typeof req.session.user !== "undefined" && req.session.user) {
            return next();
        }
        res.redirect("/login");
    };
}