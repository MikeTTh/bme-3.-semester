/**
 * Rendereli a megadott template-et.
 */
module.exports = function (objRepo, page) {
    return function (req, res, next) {
        let obj = {};
        Object.assign(obj, req.session);
        Object.assign(obj, res.locals);
        res.render(page, obj);
    };
}