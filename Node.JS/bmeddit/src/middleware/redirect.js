/**
 * Egyszerű átirányítás lesz majd itten.
 */
module.exports = function (path) {
    return function (req, res, next) {
        return res.redirect('/');
    };
}