const needs = require("../needs");
const bcrypt = require("bcrypt");
/**
 * Regisztrálja, majd bejelentkezteti a felhasználót, adatbázis, sütik, meg minden... 🍪
 */

module.exports = function (objRepo) {
    const UserModel = objRepo.UserModel;

    return function (req, res, next) {
        res.locals.register = true;
        if(req.method !== "POST") {
            return next();
        }
        
        const refill = ["email", "username"];
        refill.forEach(v => {
            if (needs(req.body, v)) {
                res.locals[v] = req.body[v];
            }
        });
        
        
        if (!needs(req.body, "email", "password", "username")) {
            res.locals.error = "Ki kell tölteni miden mezőt"
            return next();
        }
        if (req.body.password.length < 6) {
            res.locals.error = 'A jelszónak minmum 6 karakteresnek kell lenni';
            return next();
        }

        UserModel.findOne({email: req.body.email}, (err, user1) => {
            if (err) {
                return next(err);
            }
            if (user1) {
                res.locals.error = 'Már van ilyen email cím.';
                return next();
            }

            UserModel.findOne({username: req.body.username}, (err, user2) => {
                if (err) {
                    return next(err);
                }
                if (user2) {
                    res.locals.error = 'Már van ilyen felhasználónév.';
                    return next();
                }

                //nincs még ilyen email címmel user
                const newUser = new UserModel();
                newUser.email = req.body.email;
                newUser.pwHash = req.body.password;
                newUser.username = req.body.username;
                
                bcrypt.genSalt(10, (err, salt) => {
                    if (err)
                        return next(err);
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        if (err)
                            return next(err);

                        newUser.pwHash = hash;

                        newUser.save((err2) => {
                            if (err2) {
                                return next(err2);
                            }
                            req.session.user = newUser;
                            req.session.save(err => {
                                if (err)
                                    return next(err);
                                return res.redirect('/');
                            });
                            
                        });
                    });
                });


            });
        });
    };
}