const needs = require("../needs");
/**
 * Egy post mentése
 */
module.exports = function (objRepo) {
    const Post = objRepo.PostModel;
    const User = objRepo.UserModel;

    return function (req, res, next) {
        if (!needs(req.body, "title", "text")) {
            return next();
        }

        let id = null;
        let newPost = false;
        
        if (needs(req.params, "id")) {
            id = req.params.id;
        }

        Post.findOne({_id: id}, (err, post) => {
            if (err)
                next(err);
            
            if (post === null) {
                post = new Post();
                newPost = true;
            }

            post.title = req.body.title;
            post.text = req.body.text;
            post.date = new Date();
            
            if (needs(req.body, "deleteImg") && req.body.deleteImg === "on") {
                post.image = undefined;
            }

            if (needs(req, "file") && needs(req.file, "path")) {
                post.image = req.file.path.slice("static/".length);
            }

            if (newPost)
                post.user = req.session.user._id;

            post.save(err => {
                if (err) {
                    return next(err);
                }

                if (newPost) {
                    req.session.user.posts.push(post.id);

                    User.updateOne({_id: req.session.user._id}, {$push: {posts: post.id}}, (err, done) => {
                        if (err)
                            return next(err);
                        return res.redirect('/');
                    });
                } else {
                    return res.redirect('/');
                }
            });
        });
    };
}