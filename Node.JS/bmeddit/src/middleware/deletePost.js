const needs = require("../needs");
/**
 * Egy post törlése
 */
module.exports = function (objRepo) {
    const Post = objRepo.PostModel;
    const User = objRepo.UserModel;
    return function (req, res, next) {
        if(!needs(req.session, "user") || !needs(req.params, "id")) {
            return res.redirect("/"); // TODO: error site
        }
        
        Post.deleteOne({ _id: req.params.id }, (err, ok) => {
            if (err) {
                next(err);
            }
            
            if(ok) {
                User.updateOne({_id: req.session.user._id}, {$pullAll: {posts: [req.params.id]}}, (err, done) => {
                    if (err)
                        return next(err);
                    req.session.user.posts = req.session.user.posts.filter((val, i, a) => {
                        return val !== req.params.id;
                    });
                    return res.redirect('/');
                });
            }
        });
    };
}