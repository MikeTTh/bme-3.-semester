const db = require("./mongo");

const User = db.model("User", {
    username: String,
    posts: [{
        type: db.Schema.Types.ObjectID,
        ref: "Post"
    }],
    email: String,
    pwHash: String,
});

module.exports = User;