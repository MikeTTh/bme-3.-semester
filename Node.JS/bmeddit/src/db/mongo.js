const mongoose = require('mongoose');

let host = 'localhost';

if (typeof process.env.MONGO !== "undefined" && process.env.MONGO) {
    host = process.env.MONGO;
}

mongoose.connect('mongodb://'+host+'/FLGIIG',
    {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => {
        console.log(`connected to ${host}`);
    }).catch(e => {
        console.log(e);
});

module.exports = mongoose;