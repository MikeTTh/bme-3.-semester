const db = require("./mongo");

const Post = db.model("Post", {
    title: String,
    text: String,
    image: String,
    date: Date,
    user: {
        type: db.Schema.Types.ObjectID,
        ref: "User"
    }
});

module.exports = Post;