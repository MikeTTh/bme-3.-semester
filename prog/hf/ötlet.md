# Chat alkalmazás
Házi feldatnak egy HTTP-n működő chat alkalmazást szeretnék lefejlesztani, mely kliens-szerver architektúrán alapul és http-n keresztül kommunikál.

## Szerver
Szerveroldalon a következő funkciókat valósítanám meg:

* http szerver (várhatóan a `com.sun.net.httpserver.HttpServer` osztályt és JSON-t használva)
* menti a chateket és felhasználókat (várhatóan Java Collections + JSON)
* REST API-t valósít meg, melyen a kliens(ek) be tudnak jelentkezni, tudnak regisztrálni, le tudják kérni az üzeneteiket, illetve üzeneteket tudnak írni

## Kliens
A kliens oldalon a következő funkcionalitás a cél:

* http kliens (várhatóan a `java.net.HttpURLConnection` osztály segítségével)
* kommunikál a szerverrel (regisztráció, bejelentkezés, üzenetek küldése/fogadása)
* mindehhez egy userek által könnyen kezelhető GUI-t ad, várhatóan JavaFX alapokon
* a bejelentkezés után kapott tokent menti és vissza tudja olvasni


---
title: Programozás alapjai 3 HF ötlet
author: Tóth Miklós (FLGIIG)
geometry: margin=2cm
output: pdf_document
---
