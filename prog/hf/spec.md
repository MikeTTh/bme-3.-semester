# JChat
Egy HTTP alapú chat alkalmazást fejlesztek le. Egy szerverhez csatlakozik több kliens, akik egymással tudnak chatelni.

# Szerver
A `com.sun.net.httpserver.HttpServer` segítségével fogok megvalósítani egy szerver alkalmazást.
Szerver oldalon a következő path-eken fog megvalósítani egy API-t:  

## API

### `/login`
Ide a kliens egy megfelelő JSON-t küldve visszakap egy bejelentkezési tokent,
amely egy 64 bites random szám, amit a kliens opcionálisan menthet lemezre is. Ekkor a HTTP státusz az `200 OK`.
A továbbiakban ezt a számot, mint sütit vissza kell küldeni `Session` néven a többi lekérésnél.  
Érvényes token nélkül a többi API endpoint `403 Forbidden`-t ad.

Hiba esetén pedig nem számot kap vissza, hanem a hibaüzenetet. Ekkor a HTTP státusz `401 Unauthorized`.
Hibát okozhat például egy nem létező felhasználónév vagy rossz jelszó.
 
A kliens által küldött JSON ilyen formátumú lesz:
```json
{
  "username": "Mike",
  "password": "ilikejava"
}
```   

### `/register`  
Ez az endpoint lényegében megegyezik a `/login` működésével, azonban itt új fiókot tudunk létrehozni az adott szerveren.
Az eltérés annyi, hogy itt egy username létezése okozhat hibát, vagy egy jelszó gyengesége.

### `/people`
Ez az útvonal visszaadja a felhasználók listáját és jelzi, hogy ki mikor volt utoljára online. Például:
```json
{
  "Peter": "2020-11-01T21:32:26+00:00",
  "Mike":  "2020-10-30T20:00:26+00:00",
  "Alice": "2020-10-30T21:00:26+00:00",
  "Bob":   "2020-10-29T21:00:26+00:00"
}
```

### `/conversations`
Ezen az útvonalon le tudjuk kérni, hogy milyen felhasználókkal volt már beszélgetésünk korábban,
így ez a válasz a `/people` részhalmaza. Például:
```json
{
  "Peter": "2020-11-01T21:32:26+00:00",
  "Bob":   "2020-10-29T21:00:26+00:00"
}
```

### `/conversation/$username`
Ezen az útvonalon le tudjuk kérni egy adott userrel folytatott beszélgetésünk. Ez egy üzenet objektumok listáját adja vissza.
Például:
```json
[
  {
    "sender": "Mike",
    "date":   "2020-11-01T18:34:02+02:00",
    "msg":    "Hello there"
  },
  {
    "sender": "Peter",
    "date":   "2020-11-01T18:40:00+02:00",
    "msg":    "Hi"
  }
]
```

### `/send/$username`
Itt tudunk küldeni üzenetet egy usernek. Itt csak `POST` method elfogadott és a `POST` törzse (a `POST body`) az üzenet tartalma.
Metaadatokat nem kell küldeni az üzenet mellé, hiszen a szerver a sütikből úgyis tudja, hogy ki az üzenet küldője, és az időt is ismeri.

### `/ping`
Ide tud a kliens lekéréseket küldeni, hogy frissítse, hogy mikor volt utoljára online. A szerver egyszerűen egy üres `200 OK`-val válaszol.

## Tárolás
A szerver JSON fájlokban fogja tárolni az adatokat. Opcionális cél, hogy SQL-ben is tudja tárolni.

### JSON
A JSON tárolás 2 fájlból fog állni:

#### `users.json`  

Itt egy listában eltárolom minden user felhasználónevét, jelszó hash-ét és jelszó sóját. Például:  
```json
[
  {
    "username": "Mike",
    "pwhash":   "6b3a55e0261b0304143f805a24924d0c1c44524821305f31d9277843b8a10f4e",
    "salt":     "43583475897"
  },
  {
    "username": "Alice",
    "pwhash":   "64f8590dc1c153a7569418973e62755013551bcf78b624ed4c6f766005a55a36",
    "salt":     "5645645645645"
  }
]
```

#### `conversations.json`

Itt pedig egy listában eltárolom a beszélgetésekről, hogy kik szerepelnek bennük és, hogy milyen üzeneteket küldtek:
```json
[
  {
    "users": [
      "Peter",
      "Mike"
    ],
    "messages": [
      {
        "sender": "Mike",
        "date":   "2020-11-01T18:34:02+02:00",
        "msg":    "Hello there"
      },
      {
        "sender": "Peter",
        "date":   "2020-11-01T18:40:00+02:00",
        "msg":    "Hi"
      }
    ]
  },
  {
    "users": [
      "Alice",
      "Bob"
    ],
    "messages": [
      {
        "sender": "Bob",
        "date":   "2020-11-01T18:34:02+02:00",
        "msg":    "Hi Alice"
      },
      {
        "sender": "Alice",
        "date":   "2020-11-01T18:41:00+02:00",
        "msg":    "Hello Bob"
      },
      {
        "sender": "Bob",
        "date":   "2020-11-01T18:42:02+02:00",
        "msg":    "What's up?"
      }
    ]
  }
]
```

### SQL
Ha a projekt megírása során marad időm, akkor szeretnék megvalósítani adattárólást SQL-el is.
Ez egyelőre túlmutat a feltétlenül tervezett funkcionalitáson.

\newpage
# Kliens
A kliens a megnyitásakor a bejelentkező felületre kerülünk, vagy amennyiben mentve volt korábban érvényes token, akkor azonnal az üzenetekhez.

## Bejelentkezési képernyő
![Bejelentkezési ablak](startpage.eps)

A bejelentkezési képernyőn a szerver címét, a felhasználónevünket és jelszavunkat megadva a Log in-ra kattintva tudunk bejelentkezni.
Ha a *Stay logged in* be van pipálva, akkor a szervertől kapott tokent fájlba menti a kliens, így a következő alkalommal nem kell bejelentkezni. 

\newpage
## Üzenetek
![Üzenetek](messages.eps)

Az üzenetek nézetben a bal oldalon választhatunk a korábbi beszélgetések közül,
vagy a *Start new conversation*-re kattintva új beszélgetést tudunk kezdeni.

## Egyéb információk a kliensről
A kliens nem tárol adatokat a tokenen kívül. A GUI megvalósításához JavaFX-et fogok használni.
A HTTP kommunikációhoz pedig várhatóan a `java.net.HttpURLConnection`-t fogom használni.

# Egyéb információ a projektről
A kliens és a szerver az egymás közti kommunikáció során főként JSON-t használ.
Ehhez a `org.json.simple.JSONObject`-t tervezem használni.

---
title: Programozás alapjai 3 HF specifikáció
subtitle: Java Chat alkalmazás
author: Tóth Miklós (FLGIIG)
geometry: margin=2cm
output: pdf_document
lang: hu-HU
---
