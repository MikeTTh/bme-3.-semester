#!/bin/sh
# shellcheck disable=SC2211
DRAWIO="$HOME/Applications/draw.io*.AppImage"
which draw.io > /dev/null 2> /dev/null && DRAWIO="draw.io"

$DRAWIO -x -o startpage.svg startpage.drawio
inkscape startpage.svg --export-type=eps -o startpage.eps --export-ignore-filters --export-ps-level=3

$DRAWIO -x -o messages.svg messages.drawio
inkscape messages.svg --export-type=eps -o messages.eps --export-ignore-filters --export-ps-level=3

pandoc spec.md -o spec.pdf