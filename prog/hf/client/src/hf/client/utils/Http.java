package hf.client.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import sun.misc.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * HTTP lekéréseket segítő osztály
 */
public class Http {
    private static final Charset utf8 = StandardCharsets.UTF_8;
    private static final BasicCookieStore cookieStore = new BasicCookieStore();
    private static final HttpClientContext context = HttpClientContext.create();
    
    static {
        context.setCookieStore(cookieStore);
    }
    
    private static String server = "";

    /**
     * Beállítja, hogy melyik szerverrel kommunikálunk
     * @param s a szerver címe
     */
    public static void setServer(String s){
        if (!s.contains("://")) {
            s = "http://"+s;
        }
        server = s;
    }

    /**
     * Visszaadja a szerver elérési útját
     * @return a szerver címe
     */
    public static String getServer() {
        return server;
    }

    /**
     * Beállítja az uid sütit stringből
     * @param id a süti értéke
     */
    public static void setCookie(String id) {
        BasicClientCookie cookie = new BasicClientCookie("uid", id);
        String domain = server.replaceFirst(".*://", "");
        domain = domain.replaceAll(":.*", "");
        cookie.setDomain(domain);
        cookie.setPath("/");
        cookieStore.addCookie(cookie);
        context.setCookieStore(cookieStore);
    }

    /**
     * Visszaadja az uid süti értékét
     * @return a süti értéke
     * @throws CookieNotFoundException ha nem található a süti, akkor ezt dobja
     */
    public static String getCookie() throws CookieNotFoundException {
        Cookie c = cookieStore.getCookies().stream().filter(cookie -> cookie.getName().equals("uid")).findFirst().orElseThrow(CookieNotFoundException::new);
        return c.getValue();
    }

    /**
     * Nem található az uid süti
     */
    public static class CookieNotFoundException extends Exception {
        public CookieNotFoundException(){
            super("cookie not found");
        }
    }

    /**
     * Egy HTTP lekérést végez
     * @param url az elérési út a szerveren
     * @param data a küldött adat (POST esetén)
     * @param method a HTTP method
     * @return a szerver válasza
     * @throws IOException kapcsolati hiba
     */
    private static String req(String url, String data, String method) throws IOException {
        String uri = server + url;

        HttpClient httpClient = HttpClients.createDefault();

        HttpUriRequest req = new HttpGet(uri);

        if ("POST".equals(method)) {
            HttpPost post = new HttpPost(uri);
            post.setEntity(new StringEntity(data, utf8));
            req = post;
        }


        HttpResponse response = httpClient.execute(req, context);
        HttpEntity entity = response.getEntity();

        InputStream is = entity.getContent();
        byte[] by = IOUtils.readAllBytes(is);

        return new String(by, utf8);
    }

    /**
     * POST lekérés
     * @param url az elérési út a szerveren
     * @param data a küldött adat (POST esetén)
     * @return a szerver válasza
     * @throws IOException kapcsolati hiba
     */
    public static String post(String url, String data) throws IOException {
        return req(url, data, "POST");
    }

    /**
     * GET lekérés
     * @param url az elérési út a szerveren
     * @return a szerver válasza
     * @throws IOException kapcsolati hiba
     */
    public static String get(String url) throws IOException {
        return req(url, "", "GET");
    }
}
