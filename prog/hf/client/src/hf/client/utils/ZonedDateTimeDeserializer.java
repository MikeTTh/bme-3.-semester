package hf.client.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * a GSON könyvtár alapértelmezetten nem támogatja a ZonedDateTime típúst, de bővíthető,
 * úgyhogy ez az osztály végzi el a ZonedDateTime típúsú mezők deserializálását.
 */
public class ZonedDateTimeDeserializer implements JsonDeserializer<ZonedDateTime> {
    /**
     * A deserializáló függvény
     * @param jsonElement a kapott JSON mező
     * @param type a kapott mező típúsa
     * @param jsonDeserializationContext deserializáló osztály, amennyiben rész-mezőket akarunk kiolvasni
     * @return a deserializált ZonedDateTime
     * @throws JsonParseException deserializálási hiba
     */
    @Override
    public ZonedDateTime deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        String dateStr = jsonElement.getAsString();
        return ZonedDateTime.parse(dateStr, DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }
}
