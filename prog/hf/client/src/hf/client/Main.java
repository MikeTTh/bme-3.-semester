package hf.client;

import hf.client.comm.Creds;
import hf.client.utils.Http;

import java.io.IOException;

/**
 * A program belépési pontját tartalmazó osztály
 */
public class Main {
    /**
     * A program belépési pontja
     * @param args argumentumok, nincs használva
     */
    public static void main(String[] args) {
        Creds cred = new Creds();
        try {
            cred.load();
            Http.setServer(cred.server);
            Http.setCookie(cred.cookie);
        } catch (IOException ignored) {}
        App.launch(App.class);
    }
}
