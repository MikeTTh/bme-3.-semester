package hf.client;

import hf.client.comm.Creds;
import hf.client.pages.LoginPage;
import hf.client.pages.MainPage;
import hf.client.utils.Http;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * A JavaFX alkalmazást elindító osztály
 */
public class App extends Application {
    /**
     * Az elindító függvény
     * @param primaryStage alapértelemezett ablak
     */
    @Override
    public void start(Stage primaryStage) {
        Parent root = null;
        if (!Http.getServer().equals("")) {
            try {
                String resp = Http.get("/ping");
                if (resp.equals("OK")) {
                    root = new MainPage();
                }
            } catch (Exception e) {
                LoginPage p = new LoginPage();
                p.setErrorMsg("couldn't connect to server");
                root = p;
            }
        }
        if (root == null) {
            root = new LoginPage();
        }
        primaryStage.setTitle("JChat");
        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("style.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
