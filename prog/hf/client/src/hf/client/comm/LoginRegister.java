package hf.client.comm;

import hf.client.Common;
import hf.client.utils.Http;

import java.io.IOException;

/**
 * Bejelentkezést és regisztrálást megvalósító lekérések
 */
public class LoginRegister {
    /**
     * Bejelentkezéshez és regisztrációhoz szükséges adatok
     */
    private static class LoginDetails {
        public String username;
        public String password;
        public LoginDetails(String u, String p) {
            username = u;
            password = p;
        }
    }

    /**
     * Bejelentkezést és regisztrálást megvalósító függvény
     * @param username felhasználónév
     * @param password jelszó
     * @param login ha true, akkor bejelentkezni, ha false, akkor regisztrálni próbál
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     * @throws LoginException szervertől kapott hibaüzenet
     */
    public static void doLoginRegister(String username, String password, boolean login) throws IOException, LoginException {
        LoginDetails details = new LoginDetails(username, password);
        String query = Common.getGSON().toJson(details);
        
        String url = "/register";
        if (login) {
            url = "/login";
        }

        String resp = Http.post(url, query);

        if (!resp.equals("OK")) {
            throw new LoginException(resp);
        }
    }

    /**
     * Szervertől kapott hibaüzenet továbbdobására való Exception
     */
    public static class LoginException extends Exception {
        public LoginException(String except) {
            super(except);
        }
    }
}
