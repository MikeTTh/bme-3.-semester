package hf.client.comm;

import hf.client.Common;

import java.io.*;

/**
 * A süti és server cím mentésére és visszatöltésére szolgáló osztály
 */
public class Creds {
    public String cookie;
    public String server;

    /**
     * bejelentkezési adatok mentése
     * @throws IOException fájl írási hiba
     */
    public void save() throws IOException {
        String json = Common.getGSON().toJson(this);
        FileWriter w = new FileWriter("creds.json");
        w.write(json);
        w.close();
    }

    /**
     * Bejelentkezési adatok betöltése
     * @throws IOException fájl olvasási hiba
     */
    public void load() throws IOException {
        FileReader r = new FileReader("creds.json");
        Creds c = Common.getGSON().fromJson(r, Creds.class);
        if (c.cookie != null && c.server != null && !"".equals(c.cookie) && !"".equals(c.server)) {
            cookie = c.cookie;
            server = c.server;
        } else {
            throw new IOException("empty file");
        }
        r.close();
    }
}
