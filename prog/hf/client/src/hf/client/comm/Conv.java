package hf.client.comm;

import com.google.gson.reflect.TypeToken;
import hf.client.Common;
import hf.client.utils.Http;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.ZonedDateTime;
import java.util.ArrayList;

/**
 * Egy beszélgetést lekérő osztály
 */
public class Conv {

    /**
     * Egy üzenet formátuma
     */
    public static class Message {
        public final String sender;
        public final String recipient;
        public final ZonedDateTime date;
        public final String message;
        
        public Message(String sender, String recipient, ZonedDateTime date, String message) {
            this.sender = sender;
            this.recipient = recipient;
            this.date = date;
            this.message = message;
        }
    }

    private static final Type respType = new TypeToken<ArrayList<Message>>(){}.getType();

    /**
     * a beszélgetést lekérő osztály
     * @param name a felhasználó, akivel beszélgetni szeretnénk
     * @return üzenetek listája, a beszélgetés
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    public static ArrayList<Message> getConversation(String name) throws IOException {
        String resp = Http.get("/conversation/"+name);
        return Common.getGSON().fromJson(resp, respType);
    }
}
