package hf.client.comm;

import com.google.gson.reflect.TypeToken;
import hf.client.Common;
import hf.client.utils.Http;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.ZonedDateTime;
import java.util.HashMap;

/**
 * A /people és /conversations lekéréseket végző osztály
 */
public class PeopleConversations {
    private static final Type respType = new TypeToken<HashMap<String, ZonedDateTime>>(){}.getType();

    /**
     * A kétféle lekérést végző függvény
     * @param people ha true, akkor /people, ha false, akkor /conversations lekérést csinál
     * @return Az emberek és utolsó online idejük
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    private static HashMap<String, ZonedDateTime> doPeopleConversations(boolean people) throws IOException {
        String url = "/conversations";
        if (people) {
            url = "/people";
        }

        String resp = Http.get(url);
        return Common.getGSON().fromJson(resp, respType);
    }

    /**
     * /people-t lekérő függvény
     * @return Az emberek és utolsó online idejük
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    public static HashMap<String, ZonedDateTime> getPeople() throws IOException {
        return doPeopleConversations(true);
    }

    /**
     * /conversations-t lekérő függvény
     * @return Az emberek és utolsó online idejük
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    public static HashMap<String, ZonedDateTime> getConversations() throws IOException {
        return doPeopleConversations(false);
    }

}
