package hf.client.comm;

import hf.client.utils.Http;

import java.io.IOException;

/**
 * Üzenet küldésére való osztály
 */
public class SendMsg {
    /**
     * Üzenet küldése
     * @param user címzett
     * @param msg üzenet
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    public static void send(String user, String msg) throws IOException {
        Http.post("/send/"+user, msg);
    }
}
