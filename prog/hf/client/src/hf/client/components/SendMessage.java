package hf.client.components;

import hf.client.comm.Conv;
import hf.client.comm.SendMsg;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import java.io.IOException;
import java.time.ZonedDateTime;

/**
 * Üzenet küldésére való UI Pane
 */
public class SendMessage extends HBox {
    Conversation conv;

    TextField area = new TextField();

    /**
     * Konstruktor
     * @param conv a beszélgetési Pane, hogy tudjuk frissíteni új üzenet küldésekor
     */
    public SendMessage(Conversation conv) {
        super();
        this.conv = conv;
        setPadding(new Insets(8));
        setSpacing(5);
        
        setHgrow(area, Priority.ALWAYS);

        Button send = new Button("send");
        send.setOnMouseClicked(new sendHandler());

        getChildren().add(area);
        getChildren().add(send);

        setOnKeyPressed(new EnterButtonHandler());
    }

    /**
     * Új üzenetet küldő függvény
     */
    private void sendMsg() {
        try {
            String msg = area.getText();
            area.clear();
            SendMsg.send(conv.name, msg);
            conv.main.add(new MessageBox(new Conv.Message("", conv.name, ZonedDateTime.now(), msg), true), 2, conv.main.getChildren().size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Kattintást kezelő osztály
     */
    private class sendHandler implements EventHandler<MouseEvent> {
        /**
         * Kattintásra elküldi az üzenetet
         * @param mouseEvent nincs használva
         */
        @Override
        public void handle(MouseEvent mouseEvent) {
            sendMsg();
        }
    }

    /**
     * Enter gombot kezelő osztály
     */
    private class EnterButtonHandler implements EventHandler<KeyEvent> {
        /**
         * Gombnyomást kezelő függvény, enter gombra küld
         * @param keyEvent lenyomott gomb információi
         */
        @Override
        public void handle(KeyEvent keyEvent) {
            if (keyEvent.getCode().getName().equals("Enter")) {
                sendMsg();
            }
        }
    }
}
