package hf.client.components;

import hf.client.comm.PeopleConversations;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.HashMap;

/**
 * Új beszélgetést kezdő gomb, felugró ablakban lehet választani új címzettet
 */
public class NewMessage extends Button {
    PersonList personList;
    
    Stage stage;

    /**
     * Konstruktor
     * @param pl a beszélgetések listája, hogy tudjuk frissíteni, illetve a főoldalra kitenni a beszélgetést
     */
    public NewMessage(PersonList pl) {
        super("new conversation");
        personList = pl;
        setMaxWidth(Double.MAX_VALUE);
        setOnMouseClicked(new btnHandler());
    }

    /**
     * Gombra kattintást kezelő osztály
     */
    private class btnHandler implements EventHandler<MouseEvent> {
        /**
         * Ha rákkatintunk a gombra, akkor létrehoz egy új ablakot, amiben ki tudjuk választani az új címzettet
         * @param mouseEvent nincs használva
         */
        @Override
        public void handle(MouseEvent mouseEvent) {
            NewMessageView view = new NewMessageView();
            Scene scene = new Scene(view, 300, 500);
            scene.getStylesheets().add("style.css");
            stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("JChat");
            stage.show();
        }
    }

    /**
     * Az új ablak Pane-je
     */
    private class NewMessageView extends VBox {
        /**
         * Konstruktor
         */
        public NewMessageView() {
            super();
            getChildren().add(new Label("loading"));

            Thread t = new Thread(this::getPeople);
            t.start();
        }

        /**
         * /people lekérése és a UI frissítése
         */
        private void getPeople() {
            try {
                HashMap<String, ZonedDateTime> people = PeopleConversations.getPeople();
                Platform.runLater(() -> {
                    getChildren().clear();

                    people.forEach((n, d) -> {
                        PersonCard p = new PersonCard(n, d);
                        p.setOnMouseClicked(new personClickListener(n));
                        getChildren().add(p);
                    });
                });
            } catch (IOException ignored) { }
        }

        /**
         * Egy címzettre való kattintás kezelése.
         * Létrehoz egy új beszélgetést, majd megjeleníti
         */
        private class personClickListener implements EventHandler<MouseEvent> {
            String name;

            /**
             * Konstruktor
             * @param s címzett neve
             */
            public personClickListener(String s) {
                super();
                name = s;
            }

            /**
             * Kattintásra lefutó függvény
             * @param mouseEvent nincs használva
             */
            @Override
            public void handle(MouseEvent mouseEvent) {
                personList.mainPage.setCenter(new Conversation(name));
                Thread t = new Thread(() -> personList.getConversations());
                t.start();
                stage.close();
            }
        }
    }


}
