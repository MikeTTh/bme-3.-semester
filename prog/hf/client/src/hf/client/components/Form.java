package hf.client.components;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.List;

/**
 * Több bevitei mező csoportja
 */
public class Form extends GridPane {
    private int rowCnt = 0;
    private final ArrayList<TextField> textFields = new ArrayList<>();

    /**
     * Konstruktor
     */
    public Form() {
        super();
        setAlignment(Pos.CENTER);
        setPadding(new Insets(16));
        setHgap(16);
        setVgap(8);
    }

    /**
     * Mező hozzáadása
     * @param label mező neve
     * @param password ha true, akkor ki lesz pöttyözve a beírt szöveg
     */
    public void addField(String label, boolean password) {
        add(new Label(label+": "), 0, rowCnt);
        TextField field;
        if (password)
            field = new PasswordField();
        else
            field = new TextField();
        textFields.add(field);
        add(field, 1, rowCnt);
        rowCnt++;
    }

    /**
     * Beírt szövegek lekérése
     * @return a beírt szövegek listája
     */
    public List<String> getTexts() {
        final ArrayList<String> txts = new ArrayList<>();
        textFields.forEach(t -> txts.add(t.getText()));
        return txts;
    }
}
