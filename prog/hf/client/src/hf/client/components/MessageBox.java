package hf.client.components;

import hf.client.comm.Conv;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Egy üzenetbuborékot megjelenítő Pane
 */
public class MessageBox extends VBox {
    final Conv.Message message;

    /**
     * Konstruktor
     * @param message az üzenet szövege
     * @param sentByUser ha true, akkor mi küldtük az üzenetet és más lesz a színezés
     */
    public MessageBox(Conv.Message message, boolean sentByUser) {
        super();
        this.message = message;

        setPadding(new Insets(16));

        CornerRadii radi = new CornerRadii(8);
        Insets ins = new Insets(10);

        if (sentByUser) {
            setStyle("-fx-base: lightskyblue;");
            setBackground(new Background(new BackgroundFill(Color.LIGHTSKYBLUE, radi, ins)));
        } else {
            setStyle("-fx-base: mediumspringgreen");
            setBackground(new Background(new BackgroundFill(Color.MEDIUMSPRINGGREEN, radi, ins)));
        }
        getChildren().add(new Label(message.message));
    }
}
