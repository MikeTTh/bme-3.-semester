package hf.client.components;

import hf.client.comm.Conv;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Egy beszélgetést megjelenítő Pane
 */
public class Conversation extends BorderPane {
    String name;

    GridPane main = new GridPane();
    ScrollPane scroll = new ScrollPane(main);
    AnchorPane anchorPane = new AnchorPane(scroll);

    /**
     * Konstruktor
     * @param username a user, akivel beszélgetést folytatunk
     */
    public Conversation(String username){
        super();
        name = username;

        final ColumnConstraints col1 = new ColumnConstraints(0, Control.USE_COMPUTED_SIZE, Double.MAX_VALUE);
        col1.setHgrow(Priority.ALWAYS);
        main.getColumnConstraints().add(new ColumnConstraints());
        main.getColumnConstraints().add(col1);
        
        Label l = new Label("loading...");
        l.setPadding(new Insets(16));
        main.add(l, 0, 0);

        AnchorPane.setTopAnchor(scroll, .0);
        AnchorPane.setBottomAnchor(scroll, .0);
        AnchorPane.setLeftAnchor(scroll, .0);
        AnchorPane.setRightAnchor(scroll, .0);
        
        setCenter(anchorPane);
        setBottom(new SendMessage(this));
        
        scroll.setFitToWidth(true);
        scroll.setFitToHeight(true);

        (new Timer()).schedule(new TimerTask() {
            @Override
            public void run() {
                load();
            }
        }, 0, 2000);
    }

    /**
     * Lekéri az üzeneteket, majd frissíti a UI-t
     */
    private void load() {
        try {
            ArrayList<Conv.Message> messages = Conv.getConversation(name);
            Platform.runLater(() -> {
                main.getChildren().clear();
                AtomicInteger y = new AtomicInteger();
                messages.forEach(m -> {
                    int pos = 2;
                    if (m.sender.equals(name)) {
                        pos = 0;
                    }
                    main.add(new MessageBox(m, pos == 2), pos, y.getAndIncrement());
                });
            });
        } catch (IOException ignored) {}
    }
}
