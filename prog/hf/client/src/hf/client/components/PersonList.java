package hf.client.components;

import hf.client.comm.PeopleConversations;
import hf.client.pages.MainPage;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Beszélgetések listáját megjelenítő osztály
 */
public class PersonList extends VBox {
    HashMap<String, ZonedDateTime> people = new HashMap<>();

    MainPage mainPage;

    /**
     * Konstruktor
     * @param mp Főoldal, hogy tudjuk állítani, hogy melyik beszélgetés van megjelenítve
     */
    public PersonList(MainPage mp){
        super();
        Label l = new Label("loading...");
        l.setPadding(new Insets(10));
        getChildren().add(l);
        setBorder(new Border(new BorderStroke(Color.WHITE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        mainPage = mp;
        Label sel = new Label("select a conversation on the left or start a new one");
        sel.setPadding(new Insets(10));
        mp.setCenter(sel);

        (new Timer()).schedule(new TimerTask() {
            @Override
            public void run() {
                getConversations();
            }
        }, 0, 5000);
    }

    /**
     * Felhasználóra való kattintást kezelő osztály
     */
    private class personClickListener implements EventHandler<MouseEvent> {
        String name;

        /**
         * Konstruktor
         * @param s felhasználónév
         */
        public personClickListener(String s) {
            super();
            name = s;
        }

        /**
         * Kattintásra lefutó függvény
         * @param mouseEvent nincs használva
         */
        @Override
        public void handle(MouseEvent mouseEvent) {
            mainPage.setCenter(new Conversation(name));
        }
    }

    /**
     * Lekéri a /conversations-t és frissíti a UI-t
     */
    public void getConversations() {
        try {
            people = PeopleConversations.getConversations();
            Platform.runLater(() -> {
                getChildren().clear();

                getChildren().add(new NewMessage(this));

                people.forEach((n, d) -> {
                    PersonCard p = new PersonCard(n, d);
                    p.setOnMouseClicked(new personClickListener(n));
                    getChildren().add(p);
                });
            });
        } catch (IOException ignored) { }
    }
}
