package hf.client.components;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * Egy felhasználót és utolsó online idejét megjelenítő Pane
 */
public class PersonCard extends GridPane {
    /**
     * Konstruktor
     * @param name felhasználó neve
     * @param date mikor volt utoljára online
     */
    public PersonCard(String name, ZonedDateTime date) {
        super();
        add(new Label(name), 0, 0);
        add(new Label("last online at " + date.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))), 0, 1);
        setBorder(new Border(new BorderStroke(Color.WHITE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        setPadding(new Insets(4));
    }
}
