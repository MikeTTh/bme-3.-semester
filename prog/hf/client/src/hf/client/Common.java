package hf.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hf.client.utils.ZonedDateTimeDeserializer;
import hf.client.utils.ZonedDateTimeSerializer;

import java.time.ZonedDateTime;

/**
 * Közös statikus GSON példány, ami képes ZonedDateTime használatára
 */
public class Common {
    private static final Gson gson;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeSerializer());
        gsonBuilder.registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeDeserializer());
        gsonBuilder.setPrettyPrinting();
        gson = gsonBuilder.create();
    }

    /**
     * Visszaadja közös GSON objektumot
     * @return a közös GSON objektum
     */
    public static Gson getGSON() {
        return gson;
    }
}
