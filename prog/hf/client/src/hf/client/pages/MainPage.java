package hf.client.pages;

import hf.client.components.PersonList;
import javafx.scene.layout.BorderPane;

/**
 * A főoldal
 */
public class MainPage extends BorderPane {
    /**
     * Konstruktor, elkezdi felépíteni a UI-t
     */
    public MainPage(){
        super();
        setLeft(new PersonList(this));
    }
}
