package hf.client.pages;

import hf.client.comm.Creds;
import hf.client.comm.LoginRegister;
import hf.client.components.Form;
import hf.client.utils.Http;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Separator;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.List;

/**
 * Bejelentkezési ablak
 */
public class LoginPage extends GridPane {

    Form f;
    Text exceptionField;
    CheckBox save;

    /**
     * Konstruktor, felépíti a UI-t
     */
    public LoginPage(){
        super();
        setAlignment(Pos.CENTER);
        setPadding(new Insets(16));
        setHgap(16);
        setVgap(8);

        ColumnConstraints col = new ColumnConstraints();
        col.setHalignment(HPos.CENTER);
        getColumnConstraints().add(col);
        
        int row = 0;


        Text title = new Text("JChat");
        title.setFont(Font.font(Font.getDefault().getName(), 40));
        title.setFill(Color.WHITE);
        VBox titleBox = new VBox(title);
        titleBox.setPadding(new Insets(0, 0, 20, 0));
        add(titleBox, 0, row++);

        Text login = new Text("Log in");
        login.setFill(Color.WHITE);
        login.setFont(Font.font(Font.getDefault().getName(), 20));
        VBox loginBox = new VBox(login);
        add(loginBox, 0, row++);

        Separator sep = new Separator();
        sep.setPadding(new Insets(10, 0, 30, 0));
        add(sep, 0, row++);

        f = new Form();
        f.addField("Server", false);
        f.addField("Username", false);
        f.addField("Password", true);
        add(f, 0, row++);

        save = new CheckBox("stay logged in");
        save.setSelected(true);
        add(save, 0, row++);

        VBox buttonBox = new VBox();
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setPadding(new Insets(0, 64, 0, 64));
        buttonBox.setSpacing(8);

        Button loginButton = new Button("Log in");
        loginButton.setOnMouseClicked(new LoginEventHandler());
        loginButton.setStyle("-fx-base: green;");
        loginButton.setMaxWidth(Double.MAX_VALUE);
        buttonBox.getChildren().add(loginButton);

        Button registerButton = new Button("Register");
        registerButton.setOnMouseClicked(new RegisterEventHandler());
        registerButton.setMaxWidth(Double.MAX_VALUE);
        buttonBox.getChildren().add(registerButton);

        add(buttonBox, 0, row++);

        exceptionField = new Text();
        exceptionField.setFill(Color.WHITE);
        add(exceptionField, 0, row);
        
        setOnKeyPressed(new EnterButtonHandler());
    }

    /**
     * Kiolvassa a megadott adatokat és bejelentkezik vagy regisztrál
     * @param login ha true, akkor bejelentkezni próbál, ha false, akkor regisztrálni
     */
    private void loginRegister(boolean login) {
        List<String> txts = f.getTexts();
        try {
            Http.setServer(txts.get(0));
            LoginRegister.doLoginRegister(txts.get(1), txts.get(2), login);
            if (save.isSelected()) {
                String id = Http.getCookie();
                Creds c = new Creds();
                c.server = txts.get(0);
                c.cookie = id;
                c.save();
            }
            getScene().setRoot(new MainPage());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LoginRegister.LoginException e) {
            exceptionField.setText(e.getMessage());
        } catch (Http.CookieNotFoundException ignored) {}
    }

    /**
     * Enter gombot kezelő osztály
     */
    private class EnterButtonHandler implements EventHandler<KeyEvent> {
        /**
         * Enter megnyomására megpróbál bejelentkezni
         * @param keyEvent információ a lenyomott gombról
         */
        @Override
        public void handle(KeyEvent keyEvent) {
            if (keyEvent.getCode().getName().equals("Enter")) {
                loginRegister(true);
            }
        }
    }

    /**
     * Bejeletkezés gombot kezelő osztály
     */
    private class LoginEventHandler implements EventHandler<MouseEvent> {
        /**
         * kattintásra megpróbál bejelentkezni
         * @param mouseEvent nincs használva
         */
        @Override
        public void handle(MouseEvent mouseEvent) {
            loginRegister(true);
        }
    }

    /**
     * Regisztráció gombot kezelő osztály
     */
    private class RegisterEventHandler implements EventHandler<MouseEvent> {
        /**
         * kattintásra megpróbál regisztrálni
         * @param mouseEvent nincs használva
         */
        @Override
        public void handle(MouseEvent mouseEvent) {
            loginRegister(false);
        }
    }

    /**
     * Kiír egy hibaüzenetet
     * @param msg a hibaüzenet
     */
    public void setErrorMsg(String msg) {
        exceptionField.setText(msg);
    }
}
