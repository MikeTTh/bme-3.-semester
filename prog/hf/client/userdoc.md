# Bejelentkezés

Az első megnyitáskor a következő ablak fog megjelenni:
  
![bejelentkezés](screenshots/login.png)

Itt amiután kitöltjük a mezőket, be tudunk jelentkezni
vagy tudunk regisztrálni a megfelelő gombokkal.
Amennyiben legközelebb nem szeretnénk kitölteni ezeket újra,
be tudjuk pipálni a "stay logged in" melletti checkbox-ot.
Ekkor mentésre kerül a bejelentkezésünk.

\newpage
# Főoldal
![főoldal](screenshots/main.png)

A bejelentkezés után a főoldalon találjuk magunkat,
ahol meg tudjuk nyitni a korábbi beszélgetéseinket,
vagy újat tudunk kezdeni a "new conversation" gombbal.

\newpage
# Beszélgetés
![beszélgetés](screenshots/conv.png)

Egy beszélgetés kiválasztása után látjuk a korábbi üzeneteket.
A lenti szövegdobozba tudjuk írni az új üzenetünket, amit
a "send" gombbal vagy az enter billentyű lenyomásával tudunk
elküldeni.

\newpage
# Új beszélgetés kezdése
![új beszélgetés](screenshots/newconv.png)

A "new conversation" gombra kattintva felugrik egy ablak,
ahol tudunk választani a regisztrált felhasználók közül.
Egy felhasználóra kattintva létrejön és megnyílik az új beszélgetés.



---
title: JChat kliens user dokumentáció
author: Tóth Miklós (FLGIIG)
geometry: margin=2cm
output: pdf_document
---