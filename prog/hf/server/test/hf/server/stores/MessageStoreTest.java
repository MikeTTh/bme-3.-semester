package hf.server.stores;

import hf.server.data.Conversation;
import hf.server.data.ConversationBucket;
import hf.server.data.Message;
import hf.server.data.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class MessageStoreTest {
    MessageStore m;
    UserStore u = new UserStore(new CookieStore("testcookiestore.json"), "testuserstore.json");
    User one, two;
    
    private final String filename = "testmessagestore.json";

    public MessageStoreTest() throws UserStore.LoginException, UserStore.UserNotFoundException {
        one = u.getUserFromCookie(u.register("Test1", "123456"));
        two = u.getUserFromCookie(u.register("Test2", "123456"));
    }
    
    @BeforeEach
    void setUp() {
        m = new MessageStore(filename);
        m.createConversation(one, two);
    }

    @Test
    void sendMessage() throws ConversationBucket.ConversationNotFoundException {
        String msg = "msg";
        Conversation c = m.getConversation(one, two);
        c.sendMessage(one, two, msg);
        Message m = c.messages.getLast();
        Assertions.assertEquals(m.message, msg);
        Assertions.assertEquals(one.getName(), m.sender);
    }

    @Test
    void json() throws ConversationBucket.ConversationNotFoundException, IOException {
        Conversation c = m.getConversation(one, two);
        c.sendMessage(one, two, "one");
        c.sendMessage(two, one, "two");
        m.save();

        MessageStore m2 = new MessageStore(filename);
        m2.load();
        Conversation c2 = m2.getConversation(one, two);
        Assertions.assertEquals("one", c2.messages.get(0).message);
        Assertions.assertEquals("two", c2.messages.get(1).message);
    }
}