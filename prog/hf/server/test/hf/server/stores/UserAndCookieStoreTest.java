package hf.server.stores;

import hf.server.data.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class UserAndCookieStoreTest {
    UserStore u;
    
    private final String cookieFile = "testcookiestore.json";
    private final String userFile = "testuserstore.json";

    @BeforeEach
    void setUp() {
        u = new UserStore(new CookieStore(cookieFile), userFile);
    }

    @Test
    void registerCookieLogin() throws UserStore.LoginException, UserStore.UserNotFoundException {
        String username = "Test";
        String password = "123456";
        String cookie = u.register(username, password);
        
        User user = u.getUserFromCookie(cookie);
        Assertions.assertEquals(username, user.getName());
        
        cookie = u.login(username, password);
        user = u.getUserFromCookie(cookie);
        Assertions.assertEquals(username, user.getName());
    }

    @Test
    void registerErrors() {
        Assertions.assertThrows(UserStore.LoginException.class, () -> {
            u.register("", "123456");
        });
        
        Assertions.assertThrows(UserStore.LoginException.class, () -> {
            u.register("Test2", "12345");
        });
        
        Assertions.assertThrows(UserStore.LoginException.class, () -> {
            u.register("Test2", "123456");
            u.register("Test2", "1234567");
        });
    }

    @Test
    void loginErrors() {
        Assertions.assertThrows(UserStore.LoginException.class, () -> {
            u.register("Test3", "123456");
            u.login("Test3", "1234567");
        });

        Assertions.assertThrows(UserStore.LoginException.class, () -> {
           u.login("Test4", "123456");
        });

        Assertions.assertThrows(UserStore.UserNotFoundException.class, () -> {
            u.getUserFromCookie("almafa");
        });
    }

    @Test
    void json() throws UserStore.LoginException, IOException, UserStore.UserNotFoundException {
        String[] cookies = new String[10];
        for (int i = 0; i < 10; i++) {
            cookies[i] = u.register("Test"+i, "password"+i);
        }
        u.save();

        UserStore u2 = new UserStore(new CookieStore(cookieFile), userFile);
        u2.load();
        
        for (int i = 0; i < 10; i++) {
            User user = u2.getUserFromCookie(cookies[i]);
            Assertions.assertEquals("Test"+i, user.getName());
        }
    }
}