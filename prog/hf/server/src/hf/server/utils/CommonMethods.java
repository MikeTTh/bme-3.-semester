package hf.server.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import hf.server.data.Conversation;
import hf.server.data.ConversationBucket;
import hf.server.data.User;
import hf.server.stores.MessageStore;
import hf.server.stores.UserStore;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.ZonedDateTime;


/**
 * sokat használt közös statikus függvények osztálya
 */
public class CommonMethods {
    /**
     * A közös, szépen formázó és dátumot kezelő GSON példány.
     */
    private static final Gson gson;

    /*
     * Statikus konstruktor, a GSON példányt hozza létre
     */
    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeSerializer());
        gsonBuilder.registerTypeAdapter(ZonedDateTime.class, new ZonedDateTimeDeserializer());
        gsonBuilder.setPrettyPrinting();
        gson = gsonBuilder.create();
    }

    /**
     * visszaadja a közös GSON példányt
     * @return a GSON példány
     */
    public static Gson getGSON() {
        return gson;
    }

    /**
     * Megadott helyre menti a megadott objektumot, mint JSON
     * @param location fájlnév
     * @param data mentésre váró objektum
     * @throws IOException mentési hiba
     */
    public static void saveAsJson(String location, Object data) throws IOException {
        String json = gson.toJson(data);

        FileWriter fileWriter = new FileWriter(location);
        PrintWriter writer = new PrintWriter(fileWriter);
        writer.print(json);
        writer.close();
        fileWriter.close();
    }

    /**
     * Egy üzenet paramétereit tartalmazó osztály
     */
    public static class MessageProperties {
        public final User sender;
        public final User recipient;
        public final Conversation conversation;
        public MessageProperties(User sender, User recipient, Conversation conversation){
            this.sender = sender;
            this.recipient = recipient;
            this.conversation = conversation;
        }
    }

    /**
     * Kinyeri egy lekérésből a lekérést küldő felhasználót
     * @param exchange a lekérés paraméterei
     * @return a felhasználó
     */
    public static User getUserFromRequest(HttpExchange exchange) {
        String cookie = HttpUtils.Cookie(exchange);
        try {
            return UserStore.getDefaultUserStore().getUserFromCookie(cookie);
        } catch (UserStore.UserNotFoundException e) {
            return null;
        }
    }

    /**
     * Megszerzi egy lekérésből a kezdeményező és címzett felhasználót, illetve a beszélgetésüket.
     * @param exchange a lekérés paraméterei
     * @param path a lekéréshez tartozó elérési út, hogy könnyen le lehessen vágni, hogy az címzett neve maradjon csak
     * @return az üzenet paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    public static MessageProperties getMessagePropertiesFromRequest(HttpExchange exchange, String path) throws IOException {
        String recipientName = exchange.getRequestURI().getPath().substring(path.length());

        User sender, recipient;
        sender = getUserFromRequest(exchange);
        if (sender == null) {
            HttpUtils.SendString(exchange, "You are not logged in", 403);
            return null;
        }
        try {
            recipient = UserStore.getDefaultUserStore().getUser(recipientName);
        } catch (UserStore.UserNotFoundException e) {
            HttpUtils.SendString(exchange, e.getMessage(), 400);
            return null;
        }
        Conversation conv;
        try {
            conv = MessageStore.getDefaultMessageStore().getConversation(sender, recipient);
        } catch (ConversationBucket.ConversationNotFoundException e) {
            conv = MessageStore.getDefaultMessageStore().createConversation(sender, recipient);
        }
        return new MessageProperties(sender, recipient, conv);
    }

    /**
     * Frissíti, hogy mikor volt az adott user online.
     * @param exchange a lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor vagy nem bejelentkezett felhasználó esetén dobott kivétel
     */
    public static void bumpUserLastOnline(HttpExchange exchange) throws IOException {
        User u = CommonMethods.getUserFromRequest(exchange);
        if (u == null) {
            HttpUtils.SendString(exchange, "You aren't logged in", 400);
            throw new IOException();
        }
        u.onlinePing();
    }
}
