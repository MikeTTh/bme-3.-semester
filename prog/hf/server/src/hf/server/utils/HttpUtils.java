package hf.server.utils;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.util.List;

/**
 * A HTTP üzenetek küldését és feldolgozását segítő statikus függvényeket tartalmazó osztály
 */
public class HttpUtils {

    /**
     * String küldése válaszként
     * @param exchange a lekérés paraméterei 
     * @param message a küldött string
     * @param code HTTP status code
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    public static void SendString(HttpExchange exchange, String message, int code) throws IOException {
        final byte[] out = message.getBytes();
        exchange.sendResponseHeaders(code, out.length);
        exchange.getResponseBody().write(out);
        exchange.getResponseBody().close();
    }

    /**
     * Megadja, hogy egy adott lekérés nem POST volt-e,
     * így a csak POST-tal működő lekéréseknél könnyű az ellenőrzés
     * @param exchange a lekérés paraméterei
     * @return true, ha nem POST
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    public static boolean OnlyPost(HttpExchange exchange) throws IOException {
        if (!exchange.getRequestMethod().equals("POST")){
            SendString(exchange, "Method not allowed", 405);
            return true;
        }

        return false;
    }

    /**
     * Megszerzi az uid süti értékét
     * @param exchange a lekérés paraméterei
     * @return a süti értéke
     */
    public static String Cookie(HttpExchange exchange) {
        List<String> cookies =  exchange.getRequestHeaders().get("Cookie");
        if (cookies != null) {
            for (String c : cookies) {
                if (c.startsWith("uid=")) {
                    return c.substring(4);
                }
            }
        }
        return "";
    }

    /**
     * Beállítja a megadott sütit egy évre
     * @param exchange a lekérés paraméterei
     * @param cookieName a beállítandó süti neve
     * @param data a süti értéke
     */
    public static void SendCookie(HttpExchange exchange, String cookieName, String data) {
        exchange.getResponseHeaders().add("Set-Cookie", String.format("%s=%s; Max-Age=31556952", cookieName, data));
    }
}
