package hf.server.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * a GSON könyvtár alapértelmezetten nem támogatja a ZonedDateTime típúst, de bővíthető,
 * úgyhogy ez az osztály végzi el a ZonedDateTime típúsú mezők serializálását.
 */
public class ZonedDateTimeSerializer implements JsonSerializer<ZonedDateTime> {

    /**
     * Stringgé serializálja a ZonedDateTime-ot
     * @param zonedDateTime a serializálandó érték
     * @param type a mező típúsa
     * @param jsonSerializationContext serializáló osztály, amennyiben rész-mezőket akarunk serializálni
     * @return a készített JsonElement
     */
    @Override
    public JsonElement serialize(ZonedDateTime zonedDateTime, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(zonedDateTime.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
    }
}
