package hf.server.routes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.utils.HttpUtils;
import sun.misc.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * A minimalista főoldalat kiszolgáló osztály
 */
public class Index implements HttpHandler {
    /**
     * Egy minimális, html-only kezdőlap be van építve a jar-ba (index.html),
     * ezt szolgálja ki.
     * @param exchange lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        InputStream is = getClass().getResourceAsStream("index.html");
        File f = new File("client.jar");
        String index = new String(IOUtils.readAllBytes(is), StandardCharsets.UTF_8);
        if (f.exists() && !f.isDirectory()) {
            index = index.replaceAll("<!--download-->", "<a href=\"/download/client.jar\"> Download client jar </a>");
        }
        HttpUtils.SendString(exchange, index, 200);
    }
}
