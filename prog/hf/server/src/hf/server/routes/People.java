package hf.server.routes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.stores.UserStore;
import hf.server.utils.CommonMethods;
import hf.server.utils.HttpUtils;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.HashMap;

/**
 * Összes felhasználót megadó osztály
 */
public class People implements HttpHandler {
    /**
     * A lekérést kiszolgáló függvény, JSON-t ad vissza,
     * amelyben a kulcsok a felhasználók nevei, az értékek pedig, hogy mikor voltak utoljára online
     * @param exchange lekérési paraméterek
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        HashMap<String, ZonedDateTime> respTypes = new HashMap<>();
        UserStore.getDefaultUserStore().getUsers().forEach(u -> respTypes.put(u.getName(), u.getLastOnline()));
        HttpUtils.SendString(exchange, CommonMethods.getGSON().toJson(respTypes), 200);

        CommonMethods.bumpUserLastOnline(exchange);
    }
}
