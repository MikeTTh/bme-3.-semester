package hf.server.routes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.data.Conversation;
import hf.server.data.User;
import hf.server.stores.MessageStore;
import hf.server.utils.CommonMethods;
import hf.server.utils.HttpUtils;

import java.io.IOException;
import java.util.Scanner;

/**
 * Üzenet küldését kiszolgáló osztály
 */
public class Send implements HttpHandler {

    /**
     * /send/:username-et kiszolgáló függvény, POST-ot vár a küldött üzenettel
     * @param exchange lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if(HttpUtils.OnlyPost(exchange)) return;

        CommonMethods.MessageProperties props = CommonMethods.getMessagePropertiesFromRequest(exchange, "/send/");
        if (props == null) {
            return;
        }
        Conversation conv = props.conversation;
        User sender = props.sender;
        User recipient = props.recipient;
        

        StringBuilder msg = new StringBuilder();
        Scanner scan = new Scanner(exchange.getRequestBody());
        while (scan.hasNextLine()) {
            msg.append(scan.nextLine());
            msg.append("\n");
        }
        msg.deleteCharAt(msg.length()-1);
        String msgStr = msg.toString();


        msgStr = msgStr.trim();
        
        if (msgStr.equals("")) {
            HttpUtils.SendString(exchange, "empty message", 400);
            return;
        }

        conv.sendMessage(sender, recipient, msgStr);
        MessageStore.getDefaultMessageStore().save();
        HttpUtils.SendString(exchange, "OK", 200);

        CommonMethods.bumpUserLastOnline(exchange);
    }
}
