package hf.server.routes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.data.ConversationBucket;
import hf.server.data.User;
import hf.server.stores.MessageStore;
import hf.server.stores.UserStore;
import hf.server.utils.CommonMethods;
import hf.server.utils.HttpUtils;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Azon usereket visszaadó osztály, akikkel már folytattunk beszélgetést
 */
public class Conversations implements HttpHandler {
    /**
     * A lekérést kiszolgáló függvény, JSON-t ad vissza,
     * amelyben a kulcsok a felhasználók nevei, az értékek pedig, hogy mikor voltak utoljára online
     * @param exchange lekérési paraméterek
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        User user = CommonMethods.getUserFromRequest(exchange);
        HashMap<String, ZonedDateTime> respTypes = new HashMap<>();
        Collection<User> usersWithConvos = UserStore.getDefaultUserStore().getUsers()
                .stream().filter(u -> {
                    try {
                        MessageStore.getDefaultMessageStore().getConversation(user, u);
                        return true;
                    } catch (ConversationBucket.ConversationNotFoundException e) {
                        return false;
                    }
                }).collect(Collectors.toList());
        usersWithConvos.forEach(u -> respTypes.put(u.getName(), u.getLastOnline()));
        HttpUtils.SendString(exchange, CommonMethods.getGSON().toJson(respTypes), 200);

        CommonMethods.bumpUserLastOnline(exchange);
    }
}
