package hf.server.routes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.utils.CommonMethods;
import hf.server.utils.HttpUtils;

import java.io.IOException;

/**
 * Egy user utolsó online idejének frissítésére,
 * vagy helyes bejelentkezés ellenőrzésére használható útvonal
 */
public class Ping implements HttpHandler {
    /**
     * A lekérést kiszolgáló függvény, vagy OK-t ad vissza, vagy hibát
     * @param exchange lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        CommonMethods.bumpUserLastOnline(exchange);
        HttpUtils.SendString(exchange, "OK", 200);
    }
}
