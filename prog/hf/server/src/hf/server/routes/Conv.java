package hf.server.routes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.data.Conversation;
import hf.server.utils.CommonMethods;
import hf.server.utils.HttpUtils;

import java.io.IOException;

/**
 * a /conversation/:username endpoint-ot kiszolgáló osztály
 */
public class Conv implements HttpHandler {
    /**
     * Megadja a két user közt korábban folytatott beszélgetést JSON-ban, vagy, ha nem volt létrehoz egy újat
     * @param exchange lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        CommonMethods.MessageProperties props = CommonMethods.getMessagePropertiesFromRequest(exchange, "/conversation/");
        if (props == null) {
            return;
        }
        Conversation conv = props.conversation;

        String data = CommonMethods.getGSON().toJson(conv.messages);
        HttpUtils.SendString(exchange, data, 200);

        CommonMethods.bumpUserLastOnline(exchange);
    }
}
