package hf.server.routes;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.utils.HttpUtils;
import hf.server.stores.UserStore;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Regisztrációt megvalósító osztály.
 */
public class Register implements HttpHandler {
    Gson gson = new Gson();

    /**
     * A /register-t kiszolgáló függvény, POST-ot vár és JSON-t a megfelelő formátumban.
     * @param exchange lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if (HttpUtils.OnlyPost(exchange)) return;

        RegisterDetails details = gson.fromJson(new InputStreamReader(exchange.getRequestBody()), RegisterDetails.class);

        try {
            String c = UserStore.getDefaultUserStore().register(details.username, details.password);
            HttpUtils.SendCookie(exchange, "uid", c);
            UserStore.getDefaultUserStore().save();
            HttpUtils.SendString(exchange, "OK", 200);
        } catch (UserStore.LoginException e) {
            HttpUtils.SendString(exchange, e.getMessage(), 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * A várt JSON formátuma, mint Java osztály.
     * Ilyen típúsba lesz deserializálva.
     */
    private static class RegisterDetails {
        public String username;
        public String password;
    }
}
