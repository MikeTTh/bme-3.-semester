package hf.server.routes;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hf.server.stores.CookieStore;
import hf.server.utils.CommonMethods;
import hf.server.utils.HttpUtils;
import hf.server.stores.UserStore;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Bejelentkezésért felelő útvonal
 */
public class Login implements HttpHandler {
    Gson gson = new Gson();

    /**
     * A /login-t kiszolgáló függvény, POST-ot vár és JSON-t a megfelelő formátumban.
     * @param exchange lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if (HttpUtils.OnlyPost(exchange)) return;

        LoginDetails details = gson.fromJson(new InputStreamReader(exchange.getRequestBody()), LoginDetails.class);


        String cookie;
        try {
            cookie = UserStore.getDefaultUserStore().login(details.username, details.password);
            HttpUtils.SendCookie(exchange, "uid", cookie);
            HttpUtils.SendString(exchange, "OK", 200);
            CookieStore.getDefaultCookieStore().save();
        } catch (UserStore.LoginException e) {
            HttpUtils.SendString(exchange, e.getMessage(), 400);
        }

        CommonMethods.bumpUserLastOnline(exchange);
    }

    /**
     * A várt JSON formátuma, mint Java osztály.
     * Ilyen típúsba lesz deserializálva.
     */
    private static class LoginDetails {
        public String username;
        public String password;
    }
}
