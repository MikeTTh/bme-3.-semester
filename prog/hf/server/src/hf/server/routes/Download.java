package hf.server.routes;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import sun.misc.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * A kliens jar fájljának letöltését kiszolgáló útvonal
 */
public class Download implements HttpHandler {
    /**
     * Leküldi a kliens jar fileját
     * @param exchange lekérés paraméterei
     * @throws IOException kapcsolat megszakadásakor dobott kivétel, vagy akkor is, ha a fájl nem olvasható
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        FileInputStream fr = new FileInputStream("client.jar");
        final byte[] out = IOUtils.readAllBytes(fr);
        exchange.sendResponseHeaders(200, out.length);
        exchange.getResponseBody().write(out);
        exchange.getResponseBody().close();
        fr.close();
    }
}
