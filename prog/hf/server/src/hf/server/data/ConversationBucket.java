package hf.server.data;

import java.util.LinkedList;

/**
 * Több, hasonló nevű user páros beszélgetéseit tároló vödör.
 * Az ok a vödrös tárolásra, hogy a két user szerint indexelem a vödröket tároló tárat,
 * azonban pl: aaa és bbb közti beszélgetés ugyanoda esne, mint aaab és bb közti.
 */
public class ConversationBucket {
    private final LinkedList<Conversation> conversations = new LinkedList<>();

    /**
     * Egy beszélgetés elkezdésére szolgáló függvény
     * @param one az egyik felhasználó
     * @param two a másik felhasználó
     * @return
     */
    public Conversation createConversation(User one, User two) {
        final Conversation conv = new Conversation(one, two);
        conversations.add(conv);
        return conv;
    }

    /**
     * Két user közti beszélgetést kikereső függvény
     * @param one az egyik felhasználó
     * @param two a másik felhasználó
     * @return a megtalált beszélgetés
     * @throws ConversationNotFoundException akkor dobja, ha ebben a vödörben nincs beszélgetés a két felhasználó közt
     */
    public Conversation getConversation(User one, User two) throws ConversationNotFoundException {
        // lineáris keresés, de az esetek 99%-ában csak egy beszélgetés lesz a vödörben, így nem problémás a lassúsága
        for (Conversation c: conversations) {
            if (
                c.participant1.equals(one.getName()) && c.participant2.equals(two.getName()) || 
                c.participant1.equals(two.getName()) && c.participant2.equals(one.getName())
            ) {
                return c;
            }
        }
        throw new ConversationNotFoundException();
    }

    /**
     * A beszélgetés nem található
     */
    public static class ConversationNotFoundException extends Exception {
        public ConversationNotFoundException() {
            super("conversation not found");
        }
    }
}
