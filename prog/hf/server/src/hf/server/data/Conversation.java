package hf.server.data;

import java.util.LinkedList;

/**
 * Két ember közti beszélgetést tároló osztály
 */
public class Conversation {
    public final String participant1;
    public final String participant2;
    public final LinkedList<Message> messages = new LinkedList<>();

    /**
     * Konstruktor
     * @param one az egyik, ebben a beszélgetésben lévő felhasználó
     * @param two a másik, ebben a beszélgetésben lévő felhasználó
     */
    public Conversation(User one, User two){
        participant1 = one.getName();
        participant2 = two.getName();
    }

    /**
     * Üzenetet küldő függvény
     * @param sender küldő felhasználó
     * @param recipient fogadó felhasználó
     * @param msg üzenet szövege
     */
    public void sendMessage(User sender, User recipient, String msg) {
        Message message = new Message(sender.getName(), recipient.getName(), msg);
        messages.add(message);
    }
}
