package hf.server.data;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Random;

/**
 * Egy felhasználót tároló osztály
 */
public class User {
    private static MessageDigest digest;
    private static final Random rand = new Random();

    private final String username;
    private final String passHash;
    private final String salt;
    private ZonedDateTime lastOnline = ZonedDateTime.now();

    /**
     * Jelszó hash-elésre való függvény.
     * @param pass jelszó
     * @return a kapott hash
     */
    private String hashPw(String pass) {
        createDigest();
        byte[] hashBytes = digest.digest((pass+salt).getBytes(StandardCharsets.UTF_8));
        return new String(hashBytes, StandardCharsets.UTF_8);
    }

    /**
     * Létrehoz egy SHA-256 alapú hash-előt, ha nincs.
     * A dobott hiba ignorálva van, mivel elvárható,
     * hogy minden JVM implementációban legyen SHA-256.
     */
    public static void createDigest() {
        try {
            if (digest == null) {
                digest = MessageDigest.getInstance("SHA-256");
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Frissíti, hogy a user mikor volt utoljára online
     */
    public void onlinePing() {
        lastOnline = ZonedDateTime.now();
    }

    /**
     * Konstruktor
     * @param username felhasználónév
     * @param pass jelszó
     */
    public User(String username, String pass) {
        this.username = username;

        byte[] saltBytes = new byte[10];
        rand.nextBytes(saltBytes);
        this.salt = new String(saltBytes, StandardCharsets.UTF_8);

        this.passHash = hashPw(pass);
    }

    /**
     * Getter a username-hez
     * @return a user username-e
     */
    public String getName() {
        return username;
    }

    /**
     * Getter arra, hogy mikor volt utoljára online a felhasználó
     * @return az utolsó online időpont
     */
    public ZonedDateTime getLastOnline() {
        return lastOnline;
    }

    /**
     * Jelszóellenőrző függvény
     * @param password ellenőrzött jelszó
     * @return true, ha a sózott jelszavak hash-e egyezik, false, ha nem.
     */
    public boolean checkPw(String password) {
        String newHash = hashPw(password);
        return passHash.equals(newHash);
    }
}
