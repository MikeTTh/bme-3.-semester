package hf.server.data;

import java.time.ZonedDateTime;

/**
 * Egy üzenetet tároló osztály. Csak egyszer írható.
 */
public class Message {
    public final String sender;
    public final String recipient;
    public final ZonedDateTime date;
    public final String message;

    /**
     * Konstruktor.
     * @param from küldő
     * @param to fogadó
     * @param msg üzenet
     */
    public Message(String from, String to, String msg) {
        sender = from;
        recipient = to;
        message = msg;
        date = ZonedDateTime.now();
    }
}
