package hf.server;

import com.sun.net.httpserver.HttpServer;
import hf.server.routes.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Main class, a szerver belépési pontja
 */
public class Main {
    /**
     * Main függvény, a belépési pont
     * @param args - argumentumok, amennyiben az első argumentum egy szám, akkor az lesz a portszám
     * @throws IOException - ha a webszerver indításkor hibába ütközik, akkor IOException-t dobva leáll
     */
    public static void main(String[] args) throws IOException {
        int port = 8080;
        if (args.length >= 1){
            try {
                port = Integer.parseInt(args[0]);
            } catch (Exception ignored) {}
        }

        HttpServer httpServer = HttpServer.create(new InetSocketAddress(port), 0);
        httpServer.setExecutor(Executors.newCachedThreadPool());

        // TODO: Routes
        httpServer.createContext("/login", new Login());
        httpServer.createContext("/register", new Register());
        httpServer.createContext("/people", new People());
        httpServer.createContext("/conversations", new Conversations());
        httpServer.createContext("/send", new Send());
        httpServer.createContext("/conversation", new Conv());
        httpServer.createContext("/ping", new Ping());
        httpServer.createContext("/", new Index());
        httpServer.createContext("/download", new Download());

        httpServer.start();
        System.out.printf("JChat server running on :%d\n", port);
    }
}
