package hf.server.stores;

import com.google.gson.reflect.TypeToken;
import hf.server.data.User;
import hf.server.utils.CommonMethods;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Felhasználókat tároló osztály. HashMap alapján működik, így konstans idő alatt ki lehet keresni egy User-t
 */
public class UserStore {
    private static UserStore defaultUserStore;
    private HashMap<String, User> users = new HashMap<>();
    private static final Type listType = new TypeToken<HashMap<String, User>>(){}.getType();

    private final CookieStore cookieStore;
    
    private final String location;

    /**
     * Konstruktor
     * @param cs a tárhoz tartozó süti tár
     * @param filename mentési-betöltési fájlnév
     */
    public UserStore(CookieStore cs, String filename) {
        location = filename;
        cookieStore = cs;
    }

    /**
     * Egy globális felhasználó tárat ad vissza vagy csinál, a ./users.json helyen.
     * Ennek használata jelentősen leegyszerűsíti a program működését.
     * @return a globális felhasználó tár
     */
    public static UserStore getDefaultUserStore() {
        if (defaultUserStore == null) {
            defaultUserStore = new UserStore(CookieStore.getDefaultCookieStore(), "users.json");
            try {
                defaultUserStore.load();
            } catch (Exception e) {
                System.out.println("warning: couldn't load users, starting with empty store");
            }
        }
        if (defaultUserStore.users == null)
            defaultUserStore.users = new HashMap<>();
        return defaultUserStore;
    }

    /**
     * Egy felhasználót regisztráló függvény
     * @param username felhasználónév
     * @param password jelszó
     * @return új süti
     * @throws LoginException amennyiben nem sikerült a regisztráció, akkor dobódik
     */
    public String register(String username, String password) throws LoginException {
        if (username == null || username.trim().equals("")) {
            throw new LoginException("You must enter a username");
        }

        if (password == null || password.length() < 6) {
            throw new LoginException("Password must be 6 characters or longer");
        }
        
        User u = users.get(username);
        if (u != null){
            throw new LoginException("User already exists");
        }
        

        u = new User(username, password);
        users.put(username, u);
        return createCookieForUser(u);
    }

    /**
     * Egy felhasználót bejelentkeztető függvény
     * @param username felhasználónév
     * @param password jelszó
     * @return új süti
     * @throws LoginException amennyiben nem sikerült a bejelentkezés, akkor dobódik
     */
    public String login(String username, String password) throws LoginException {
        User u = users.getOrDefault(username, null);
        if (u == null) {
            throw new LoginException("user not found");
        }
        
        if (!u.checkPw(password)) {
            throw new LoginException("password incorrect");
        }

        u.onlinePing();
        return createCookieForUser(u);
    }

    /**
     * Létrehoz egy új sütit egy felhasználóhoz
     * @param u a felhasználó
     * @return a süti
     */
    public String createCookieForUser(User u){
        return cookieStore.createCookie(u.getName());
    }

    /**
     * Tár mentése, mint JSON a konstruktorban megadott fájlba
     * @throws IOException sikertelen mentés esetén dobja
     */
    public void save() throws IOException {
        CommonMethods.saveAsJson(location, users);
        cookieStore.save();
    }

    /**
     * Betölti a tárat a konstruktorban megadott fájlból
     * @throws IOException sikertelen betöltés esetén dobódik
     */
    public void load() throws IOException {
        FileReader f = new FileReader(location);
        users = CommonMethods.getGSON().fromJson(f, listType);
        f.close();

        cookieStore.load();
    }

    /**
     * Sütiből megadja, hogy milyen felhasználót azonosít
     * @param cookie a süti
     * @return a hozzá tartozó felhasználó
     */
    public User getUserFromCookie(String cookie) throws UserNotFoundException {
        User u = users.getOrDefault(cookieStore.getUserNameFromCookie(cookie), null);
        if (u == null) {
            throw new UserNotFoundException();
        }
        return u;
    }

    /**
     * Felhasználónév alapján keres meg egy usert
     * @param name felhasználónév
     * @return a user
     * @throws UserNotFoundException ez dobodik, ha a felhasználó nem található
     */
    public User getUser(String name) throws UserNotFoundException {
        User u = users.getOrDefault(name, null);
        if (u == null) {
            throw new UserNotFoundException();
        }
        return u;
    }

    /**
     * Felhasználók listáját visszaadó függvény
     * @return a felhasználók listája
     */
    public Collection<User> getUsers() {
        return users.values();
    }

    /**
     * Bejelentkezési és regisztrációs hibák exception-je
     */
    public static class LoginException extends Exception {
        public LoginException(String msg) {
            super(msg);
        }
    }

    /**
     * A felhasználó nem található a tárban
     */
    public static class UserNotFoundException extends Exception {
        public UserNotFoundException() {
            super("user not found");
        }
    }
}
