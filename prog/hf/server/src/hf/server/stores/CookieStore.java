package hf.server.stores;

import com.google.gson.reflect.TypeToken;
import hf.server.utils.CommonMethods;

import java.io.*;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Random;

/**
 * Sütiket tároló osztály. HashMap alapú, így gyorsan, konstans idő alatt ki lehet keresni egy sütihez tartozó usert.
 */
public class CookieStore {
    private static final Type listType = new TypeToken<HashMap<String, String>>(){}.getType();
    private static CookieStore defaultCookieStore;

    private HashMap<String, String> cookies = new HashMap<>();

    private final String location;

    /**
     * Konstruktor
     * @param filename mentési-betöltési fájlnév
     */
    public CookieStore(String filename) {
        location = filename;
    }

    /**
     * Egy globális süti tárat ad vissza vagy csinál, a ./cookies.json helyen.
     * Ennek használata jelentősen leegyszerűsíti a program működését.
     * @return a globális süti tár
     */    
    public static CookieStore getDefaultCookieStore() {
        if (defaultCookieStore == null) {
            defaultCookieStore = new CookieStore("cookies.json");
            try {
                defaultCookieStore.load();
            } catch (Exception e) {
                System.out.println("warning: couldn't load cookies, starting with empty store");
            }
        }
        if (defaultCookieStore.cookies == null)
            defaultCookieStore.cookies = new HashMap<>();
        return defaultCookieStore;
    }

    /**
     * Süti létrehozása egy adott userhez
     * @param username a user, akit azonosít a süti
     * @return az új süti
     */
    public String createCookie(String username){
        Random random = new Random();
        String cookieStr = Integer.toString(random.nextInt());
        while (getUserNameFromCookie(cookieStr) != null) {
            cookieStr = Integer.toString(random.nextInt());
        }
        cookies.put(cookieStr, username);
        return cookieStr;
    }

    /**
     * Tár mentése, mint JSON a konstruktorban megadott fájlba
     * @throws IOException sikertelen mentés esetén dobja
     */
    public void save() throws IOException {
        CommonMethods.saveAsJson(location, cookies);
    }

    /**
     * Sütiből megadja, hogy milyen felhasználót azonosít
     * @param cookie a süti
     * @return a hozzá tartozó user
     */
    public String getUserNameFromCookie(String cookie){
        return cookies.getOrDefault(cookie, null);
    }

    /**
     * Betölti a tárat a konstruktorban megadott fájlból
     * @throws IOException sikertelen betöltés esetén dobódik
     */
    public void load() throws IOException {
        FileReader f = new FileReader(location);
        cookies = CommonMethods.getGSON().fromJson(f, listType);
        f.close();
    }

}
