package hf.server.stores;

import com.google.gson.reflect.TypeToken;
import hf.server.data.Conversation;
import hf.server.data.ConversationBucket;
import hf.server.data.User;
import hf.server.utils.CommonMethods;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

/**
 * Üzeneteket tároló osztály. Vödrös hash alapon működik, így nagyjából konstans idő alatt ki lehet keresni egy beszélgetést.
 */
public class MessageStore {
    private static MessageStore defaultMessageStore;
    private static final Type listType = new TypeToken<HashMap<String, ConversationBucket>>(){}.getType();
    private HashMap<String, ConversationBucket> buckets = new HashMap<>();
    private final String location;

    /**
     * Egy globális üzenet tárat ad vissza vagy csinál, a ./messages.json helyen.
     * Ennek használata jelentősen leegyszerűsíti a program működését.
     * @return a globális üzenet tár
     */
    public static MessageStore getDefaultMessageStore() {
        if (defaultMessageStore == null) {
            defaultMessageStore = new MessageStore("messages.json");
            try {
                defaultMessageStore.load();
            } catch (IOException e) {
                System.out.println("warning: couldn't load messages, starting with empty store");
            }
        }
        return defaultMessageStore;
    }

    /**
     * Konstruktor
     * @param filename mentési-betöltési fájlnév
     */
    public MessageStore(String filename) {
        location = filename;
    }

    /**
     * Tár mentése, mint JSON a konstruktorban megadott fájlba
     * @throws IOException sikertelen mentés esetén dobja
     */
    public void save() throws IOException {
        CommonMethods.saveAsJson(location, buckets);
    }

    /**
     * Betölti a tárat a konstruktorban megadott fájlból
     * @throws IOException sikertelen betöltés esetén dobódik
     */
    public void load() throws IOException {
        FileReader f = new FileReader(location);
        buckets = CommonMethods.getGSON().fromJson(f, listType);
        f.close();
    }

    /**
     * Beszélgetés létrehozása kt felhasználó közt
     * @param one az egyik felhasználó
     * @param two a másik felhasználó
     * @return a létrejött beszélgetés
     */
    public Conversation createConversation(User one, User two) {
        ConversationBucket bucket = new ConversationBucket();
        Conversation conv = bucket.createConversation(one, two);
        
        buckets.put(generateKey(one, two), bucket);

        try {
            save();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return conv;
    }

    /**
     * Visszaadja két user közt korábban folytatott beszélgetést
     * @param one az egyik felhasználó
     * @param two a másik felhasználó
     * @return a korábbi beszélgetés
     * @throws ConversationBucket.ConversationNotFoundException nem volt még beszélgetésük
     */
    public Conversation getConversation(User one, User two) throws ConversationBucket.ConversationNotFoundException {
        ConversationBucket bucket = buckets.getOrDefault(generateKey(one, two), null);
        if (bucket == null) {
            throw new ConversationBucket.ConversationNotFoundException();
        }
        return bucket.getConversation(one, two);
    }

    /**
     * A HashMap kulcsát generáló függvény.
     * A két user nevét összefűzi abc sorrendben
     * @param one az egyik felhasználó
     * @param two a másik felhasználó
     * @return a kulcs
     */
    public static String generateKey(User one, User two) {
        if (one.getName().compareTo(two.getName()) <= 0) {
            return one.getName()+two.getName();
        } else {
            return two.getName()+one.getName();
        }
    }
}
