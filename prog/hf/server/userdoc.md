# A szerver üzembe helyezése

Egy build után csak futtatni kell a jar file-t:
```sh
java -jar server.jar
```
Ezután megjelenik a terminálban a következő üzenet:
```
JChat server running on :8080
```

# Más port megadása
Az első argumentumban tudunk megadni egy alternatív portot:
```sh
java -jar server.jar 80
```
Ilyenkor ezen a porton fog futni a http szerver.

# Tárolás
A szerver a `users.json`-ba menti a felhasználókat,
a `cookies.json`-ba a sütiket,
és a `messages.json`-ba az üzeneteket.

---
title: JChat szerver user dokumentáció
author: Tóth Miklós (FLGIIG)
geometry: margin=2cm
output: pdf_document
---
