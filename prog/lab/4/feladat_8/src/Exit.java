public class Exit extends Command {
    public Exit(CollectionWrapper beers) {
        super("exit", beers);
    }

    @Override
    public void run(String[] args) {
        System.out.println("goodbye.");
        System.exit(0);
    }
}
