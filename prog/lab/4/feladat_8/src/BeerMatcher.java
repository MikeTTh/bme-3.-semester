public interface BeerMatcher {
    boolean match(Beer b, String o);
}
