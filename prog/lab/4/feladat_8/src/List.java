public class List extends Command {
    public List(CollectionWrapper beers) {
        super("list", beers);
    }

    @Override
    public void run(String[] args) {
        for(int i = args.length-1; i >= 0; i--) {
            if (args[i].equals("name")) {
                beers.beers.sort(new NameComparator());
            } else if (args[i].equals("style")) {
                beers.beers.sort(new StyleComparator());
            } else if (args[i].equals("strength")) {
                beers.beers.sort(new StrengthComparator());
            }
        }

        for (Beer b : beers.beers) {
            System.out.println(b);
        }
    }
}
