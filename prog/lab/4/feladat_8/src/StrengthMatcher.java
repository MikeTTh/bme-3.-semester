public class StrengthMatcher implements BeerMatcher {
    boolean full;
    public StrengthMatcher(boolean full) {
        this.full = full;
    }


    @Override
    public boolean match(Beer b, String o) {
        try {
            double strength = Double.parseDouble(o);
            if (full) {
                return b.getStrength() == strength;
            }
            return b.getStrength() >= strength;
        } catch (Exception e) {
            return false;
        }
    }
}
