import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class Load extends Command {
    public Load(CollectionWrapper beers) {
        super("load", beers);
    }

    @Override
    public void run(String[] args) {
        if (args.length < 1) {
            System.out.println("usage: load <file>");
            return;
        }

        try {
            FileInputStream f = new FileInputStream(args[0]);
            ObjectInputStream ois = new ObjectInputStream(f);
            beers.beers = (ArrayList<Beer>) ois.readObject();
            ois.close();
            f.close();
        } catch (IOException e) {
            System.out.println("I/O error");
        } catch (ClassNotFoundException e) {
            System.out.println("Internal error");
        }
    }
}
