public class StyleMatcher implements BeerMatcher {
    private final boolean full;

    public StyleMatcher(boolean full){
        super();
        this.full = full;
    }

    @Override
    public boolean match(Beer b, String o) {
        if (full) {
            return b.getStyle().equals(o);
        } else {
            return b.getStyle().contains(o);
        }
    }
}
