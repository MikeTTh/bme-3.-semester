import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        CollectionWrapper beers = new CollectionWrapper();
        Command[] commands = {new Exit(beers), new Add(beers), new List(beers), new Save(beers), new Load(beers), new Search(beers), new Find(beers)};

        Scanner scan = new Scanner(System.in);
        printPrompt();
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            String[] arg = line.split(" ");
            boolean ran = false;

            for (Command c : commands) {
                if (c.getName().equals(arg[0])) {
                    c.run(Command.cutArgs(arg));
                    ran = true;
                }
            }
            if (!ran && arg[0].length() != 0) {
                System.out.printf("command \"%s\" not found\n", arg[0]);
            }

            printPrompt();
        }
        System.out.println("goodbye.");
    }

    static void printPrompt() {
        System.out.print("BeerShell => ");
    }
}
