public class Add extends Command {
    public Add(CollectionWrapper beers) {
        super("add", beers);
    }

    @Override
    public void run(String[] args) {
        if (args.length < 3) {
            System.out.println("usage: add <name> <style> <strength>");
            return;
        }
        try {
            double strength = Double.parseDouble(args[2]);
            beers.beers.add(new Beer(
                    args[0],
                    args[1],
                    strength
            ));
        } catch (NumberFormatException e) {
            System.out.println("strength needs to be a number");
        }
    }
}
