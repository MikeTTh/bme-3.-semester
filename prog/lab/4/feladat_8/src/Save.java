import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Save extends Command {
    public Save(CollectionWrapper beers) {
        super("save", beers);
    }

    @Override
    public void run(String[] args) {
        if (args.length < 1) {
            System.out.println("usage: save <filename>");
            return;
        }
        try {
            FileOutputStream f =  new FileOutputStream(args[0]);
            ObjectOutputStream oos = new ObjectOutputStream(f);
            oos.writeObject(beers.beers);
            oos.close();
            f.close();
        } catch (IOException e) {
            System.out.println("error writing file");
        }

    }
}
