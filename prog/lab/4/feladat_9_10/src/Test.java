public class Test {

    public static void main(String[] args) {
        PQueue<String> q = new PQueue<>();
        q.push("alma");
        q.push("körte");
        q.push("citrom");
        q.push("barack");

        try {
            System.out.println(q.top());
            System.out.println();

            for (String i : q) {
                System.out.println(i);
            }

            System.out.println();

            while(q.size() > 0) {
                System.out.println(q.pop());
            }
        } catch (EmptyQueueException e) {
            e.printStackTrace();
        }

        System.out.println();

        PQueue<Integer> s = new PQueue<Integer>();
        s.push(1); s.push(2); s.push(3); s.push(4);
        for (Integer i : s) {
            System.out.println(i);
        }
    }
}
