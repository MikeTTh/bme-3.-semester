import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class PQueue<T extends Comparable> implements Iterable<T> {
    ArrayList<T> tar = new ArrayList<>();
    public PQueue() {
        super();
    }

    void push(T t) {
        tar.add(t);
    }

    int size() {
        return tar.size();
    }

    void clear() {
        tar.clear();
    }


    T pop() throws EmptyQueueException {
        T bigg = top();
        tar.remove(bigg);
        return bigg;
    }

    T top() throws EmptyQueueException {
        if (size() <= 0) {
            throw new EmptyQueueException();
        }
        return (T) Collections.max(tar);
    }


    @Override
    public Iterator<T> iterator() {
        Collections.sort(tar, Collections.reverseOrder());
        return tar.iterator();
    }
}
