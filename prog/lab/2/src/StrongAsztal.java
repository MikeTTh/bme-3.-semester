import java.util.ArrayList;
import java.util.Random;

/**
 * Az Asztal, ahol a Jatekosok ülnek.
 */
public class StrongAsztal extends Asztal {
    private Jatekos[] jatekosok = new Jatekos[10];
    private int jatekosNum = 0;
    private double goal;
    private boolean done = false;

    /**
     * Játékos leültetése az Asztalhoz
     * @param j A leülni vágyó Jatekos
     * @throws TeleAsztal kivétel, ha tele az Asztal
     */
    @Override
    public void addJatekos(Jatekos j) throws TeleAsztal {
        if (jatekosNum >= 10)
            throw new TeleAsztal();

        j.setAsztal(this);
        jatekosok[jatekosNum++] = j;
    }

    /**
     * Elindít egy új játékot.
     */
    @Override
    public void ujJatek() {
        tet = 0;
        kor = 1;
        Random r = new Random();
        goal = r.nextDouble()*100;
        done = false;
    }

    /**
     * Lejátszat egy kört.
     * @throws NincsJatekos kivétel, ha nincs Jatekos az Asztalnál.
     */
    @Override
    public void kor() throws NincsJatekos {
        if (jatekosNum == 0)
            throw new NincsJatekos();

        if (done) {
            System.out.println("Vége a játéknak.");
            return;
        }

        for (int i = 0; i < jatekosNum; i++) {
            Jatekos j = jatekosok[i];
            j.lep();
            if (tet > goal) {
                if (tet < goal*1.1) {
                    System.out.printf("A %d. játékos nyert.\n", i+1);
                } else {
                    System.out.printf("A %d. játékos vesztett.\n", i+1);
                }
                done = true;
            }
        }
        System.out.printf("tet=%g\n", tet);
        kor++;
    }
}
