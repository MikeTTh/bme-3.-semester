/**
 * Félős Jatekost szimuláló osztály.
 */
public class Nyuszi extends Jatekos {
    String szín;

    /**
     * Konstruktor.
     * @param sz Nyuszi színe.
     */
    public Nyuszi(String sz) {
        super();
        szín = sz;
    }

    @Override
    public void lep() {
        System.out.printf("Szín: %s, kör: %d\n", szín, a.getKor());
        if (a.getTet() < 50) {
            a.emel(5);
        } else {
            System.out.printf("Húha! A tét már %f!", a.getTet());
        }
    }

    @Override
    public String toString() {
        return szín;
    }
}
