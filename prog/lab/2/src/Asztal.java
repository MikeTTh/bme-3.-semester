import java.util.ArrayList;
import java.util.Random;

/**
 * Az Asztal, ahol a Jatekosok ülnek.
 */
public abstract class Asztal {
    protected int kor;
    protected double tet;

    /**
     * Játékos leültetése az Asztalhoz
     * @param j A leülni vágyó Jatekos
     * @throws TeleAsztal kivétel, ha tele az Asztal
     */
    public void addJatekos(Jatekos j) throws TeleAsztal {}

    /**
     * Elindít egy új játékot.
     */
    public void ujJatek() {}

    /**
     * Lejátszat egy kört.
     * @throws NincsJatekos kivétel, ha nincs Jatekos az Asztalnál.
     */
    public void kor() throws NincsJatekos {}

    /**
     * Visszaadja, hogy hányadik körben vagyunk.
     * @return kör száma.
     */
    public int getKor() {
        return kor;
    }

    /**
     * Emeli az Asztal tétét.
     * @param mivel emelés mennyisége.
     */
    public void emel(double mivel){
        tet += mivel;
    }

    /**
     * Visszaadja az Asztal jelenlegi tétét.
     * @return a tét.
     */
    public double getTet() {
        return tet;
    }
}
