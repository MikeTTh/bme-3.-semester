/**
 * Belépési pont, event loop.
 */
public class Main {
    /**
     * main függvény, belépési pont.
     * @param args argumentumok, nincsenek használva.
     */
    public static void main(String[] args) {
        {
            int num = 999999;
            /*
            A WeakReference-eknél elkezdtek felszbadulni a Jatekos-ok amikor kifutottunk memóriából és NullPointerException-okat kaptunk.
            * */
            WeakAsztal[] asztals = new WeakAsztal[num];
            for (int i = 0; i < num; i++){
                asztals[i] = new WeakAsztal();
                for (int j = 0; j < 10; j++) {
                    try {
                        asztals[i].addJatekos(new Mester(5));
                    } catch (Exception e) {
                        System.out.println("oof");
                    }
                }
            }
            System.gc();
            for (int i = 0; i < num; i++){
                try {
                    asztals[i].kor();
                } catch (NincsJatekos j) {
                    System.out.println(j);
                }
            }
            try {
                System.out.println("\uD83D\uDECC");
                Thread.sleep(1000);
            } catch (Exception e) {}
        }


        StrongAsztal a = new StrongAsztal();

        a.ujJatek();

        try {
            a.addJatekos(new Mester(5));
            a.addJatekos(new Nyuszi("hupilila"));
            a.addJatekos(new Ember());
        } catch (TeleAsztal t) {
            System.out.println(t);
        }
        for (int i = 0; i < 10; i++) {
            try {
                a.kor();
            } catch (NincsJatekos n) {
                System.out.println(n);
            }
        }

        a = null;
        System.gc();

        try {
            Thread.sleep(1000);
        } catch (Exception e) {}

    }
}
