/**
 * A Jatekos osztály egy Asztalnál ülő kaszinó játékos.
 * */
public abstract class Jatekos {
    /**
     * Az Asztal, amelynél a Jatekos ül.
     * */
    protected StrongAsztal a;

    /**
     * Setter az "a" Asztalhoz.
     * */
    public void setAsztal(StrongAsztal a) {
        this.a = a;
    }

    /**
     * A játékos lépésének működése, minden körben egyszer meghívódik.
     * */
    public void lep() {
        System.out.printf("%d. körnél tartunk, a tét %f\n", a.getKor(), a.getTet());
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.printf("Azonosító: %d\n", this.hashCode());
        System.out.println(this);
    }
}
