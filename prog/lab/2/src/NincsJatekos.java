/**
 * Kivétel, ami akkor dobódik, ha nincs Jatekos az Asztalnál.
 */
public class NincsJatekos extends Exception {
    public NincsJatekos() {
        super("Nincs játékos");
    }
}
