/**
 * Mester Jatekost szimuláló osztály.
 */
public class Mester extends Jatekos {
    /**
     * Mesteri fokozat.
     */
    private int fokozat;

    /**
     * Konstruktor.
     * @param fok mesteri fokozat.
     */
    public Mester(int fok) {
        super();
        fokozat = fok;
    }

    @Override
    public void lep() {
        System.out.printf("fokozat: %d, kör: %d\n", fokozat, a.getKor());
        a.emel(fokozat);
    }

    @Override
    public String toString() {
        return "Mester"+fokozat;
    }
}
