/**
 * Egy kezdő Jatekost szimulál.
 */
public class Kezdo extends Jatekos {
    String nev;

    /**
     * Konstruktor.
     * @param n név.
     */
    public Kezdo(String n) {
        super();
        nev = n;
    }

    @Override
    public void lep() {
        System.out.printf("%s: %d. kör\n", this, a.getKor());
        if (a.getKor() % 2 != 1) {
            a.emel(1);
        }
    }

    @Override
    public String toString() {
        return nev;
    }
}
