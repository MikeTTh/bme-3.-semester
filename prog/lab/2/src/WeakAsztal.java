import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Random;

public class WeakAsztal extends Asztal {
    private int jatekosNum = 0;
    WeakReference<Jatekos> jatekosok[];
    private double goal;
    private boolean done = false;

    public WeakAsztal() {
        super();
        jatekosok = (WeakReference<Jatekos>[]) new WeakReference[10];
        Arrays.fill(jatekosok, new WeakReference<Jatekos>(null));
    }

    @Override
    public void addJatekos(Jatekos j) throws TeleAsztal {
        jatekosok[jatekosNum++] = new WeakReference<Jatekos>(j);
    }

    /**
     * Elindít egy új játékot.
     */
    @Override
    public void ujJatek() {
        tet = 0;
        kor = 1;
        Random r = new Random();
        goal = r.nextDouble()*100;
        done = false;
    }

    /**
     * Lejátszat egy kört.
     * @throws NincsJatekos kivétel, ha nincs Jatekos az Asztalnál.
     */
    @Override
    public void kor() throws NincsJatekos {
        if (jatekosNum == 0)
            throw new NincsJatekos();

        if (done) {
            System.out.println("Vége a játéknak.");
            return;
        }

        for (int i = 0; i < jatekosNum; i++) {
            WeakReference<Jatekos> j = jatekosok[i];
            try {
                j.get().lep();
            } catch (NullPointerException e) {
                System.out.println("Fel lett szabadítva egy játékos");
                done = true;
                return;
            }
            if (tet > goal) {
                if (tet < goal*1.1) {
                    System.out.printf("A %d. játékos nyert.\n", i+1);
                } else {
                    System.out.printf("A %d. játékos vesztett.\n", i+1);
                }
                done = true;
            }
        }
        System.out.printf("tet=%g\n", tet);
        kor++;
    }
}