/**
 * Egy botot implementáló osztály.
 */
public class Robot extends Jatekos {
    private static int maxID = 0;
    private int id;

    /**
     * Konstruktor.
     */
    public Robot() {
        super();
        id = maxID++;
    }

    @Override
    public void lep() {
        System.out.printf("%s, %d. kör\n", this, a.getKor());
    }

    @Override
    public String toString() {
        return "Robot"+id;
    }
}
