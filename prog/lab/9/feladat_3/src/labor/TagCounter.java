package labor;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class TagCounter extends org.xml.sax.helpers.DefaultHandler {
    private double lat;
    private double lon;
    
    private final Map<Double, BusStop> busStops = new TreeMap<>();

    private final Map<String, Integer> cnt = new LinkedHashMap<>();
    private BusStop lastStop = new BusStop(0,0);
    
    public TagCounter(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        cnt.put(qName, cnt.getOrDefault(qName, 0)+1);
        if (qName.equals("node")) {
            lastStop = new BusStop(Double.parseDouble(attributes.getValue("lat")), Double.parseDouble(attributes.getValue("lon")));
        }
        lastStop.parseTag(qName, attributes);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("node") && lastStop.isBusStop) {
            busStops.put(lastStop.distance, lastStop);
        }
    }

    @Override
    public void endDocument() throws SAXException {
        cnt.forEach((k, v) -> {
            System.out.printf("%s: %d\n", k, v);
        });

        System.out.println();

        busStops.forEach((k, v) -> {
            v.print();
        });
    }
    
    private class BusStop {
        String name;
        String oldname;
        String wheelchair;
        boolean isBusStop = false;
        double distance;
        
        public BusStop(double la, double lo){
            distance = dist1(lat, lon, la, lo);
        }

        public void print() {
            if (isBusStop) {
                System.out.printf("Megálló: %s", name);
                if(oldname != null) {
                    System.out.printf(" (%s)", oldname);
                }
                System.out.printf("\n\tKerekesszék: %s\n", wheelchair);
                System.out.printf("\tTáv: %f\n", distance);
            }
        }

        public void parseTag(String qName, Attributes attributes) {
            if (qName.equals("tag")) {
                if (attributes.getValue("v").equals("bus_stop")) {
                    isBusStop = true;
                } else {
                    String v = attributes.getValue("v");
                    switch (attributes.getValue("k")) {
                        case "name":
                            name = v;
                            break;
                        case "old_name":
                            oldname = v;
                            break;
                        case "wheelchair":
                            wheelchair = v;
                            break;
                    }
                }
            }
        }

        double dist1(double lat1, double lon1, double lat2, double lon2) {
            double R = 6371000;
            double phi1 = Math.toRadians(lat1);
            double phi2 = Math.toRadians(lat2);
            double dphi = phi2 - phi1;
            double dl = Math.toRadians(lon2 - lon1);
            double a = Math.sin(dphi / 2) * Math.sin(dphi / 2) + Math.cos(phi1) * Math.cos(phi2) * Math.sin(dl / 2) * Math.sin(dl / 2);
            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double d = R * c;
            return d;
        }

        /*double dist2(double lat1, double lon1, double lat2, double lon2) {
            double R = 6371000;
            double phi1 = Math.toRadians(lat1);
            double phi2 = Math.toRadians(lat2);
            double dl = Math.toRadians(lon2 - lon1);
            double d = Math.acos(Math.sin(phi1) * Math.sin(phi2) + Math.cos(phi1) * Math.cos(phi2) * Math.cos(dl)) * R;
            return d;
        }*/
    }
}
