package labor;

import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class Main {
    
    public static void main(String[] args) {
        String filename = "";
        String lastFlag = "";
        double lat = .0, lon = .0;
        for (String flag: args) {
            switch (lastFlag) {
                case "-i":
                    filename = flag;
                    break;
                case "-lat":
                    lat = Double.parseDouble(flag);
                    break;
                case "-lon":
                    lon = Double.parseDouble(flag);
                    break;
            }
            
            lastFlag = flag;
        }
        
        
        DefaultHandler h = new TagCounter(lat, lon);
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser p = factory.newSAXParser();
            p.parse(new File(filename), h);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
