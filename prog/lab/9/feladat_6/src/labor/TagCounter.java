package labor;

import org.jdom2.Element;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TagCounter {

    private final Map<String, Integer> cnt = new LinkedHashMap<>();

    public TagCounter(Element element) {
        cnt.put(element.getName(), 1);
        countTags(element.getChildren());
    }
    
    private void countTags(List<Element> elements) {
        elements.forEach(e -> {
            cnt.put(e.getName(), cnt.getOrDefault(e.getName(), 0)+1);
            countTags(e.getChildren());
        });
    }
    
    public void printCnt() {
        cnt.forEach((k, v) -> {
            System.out.printf("%s: %d\n", k, v);
        });
    }
}
