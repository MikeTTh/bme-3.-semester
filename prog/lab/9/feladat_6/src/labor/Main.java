package labor;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileOutputStream;

public class Main {

    public static void main(String[] args) {
        String filename = null;
        String outname = null;
        double lat = .0, lon = .0;
        String lastFlag = "";
        for (String arg : args) {
            switch (lastFlag) {
                case "-i":
                    filename = arg;
                    break;
                case "-o":
                    outname = arg;
                    break;
                case "-lat":
                    lat = Double.parseDouble(arg);
                    break;
                case "-lon":
                    lon = Double.parseDouble(arg);
                    break;
            }

            lastFlag = arg;
        }
        assert filename != null;
        assert outname != null;


        try {
            SAXBuilder saxBuilder = new SAXBuilder();

            File input = new File(filename);
            Document doc = saxBuilder.build(input);

            TagCounter cnt = new TagCounter(doc.getRootElement());
            cnt.printCnt();

            BusStopFilter filter = new BusStopFilter(doc, lat, lon);
            Document newDoc = filter.getDocument();

            XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());

            FileOutputStream f = new FileOutputStream(outname);
            xmlOutputter.output(newDoc, f);
            f.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
