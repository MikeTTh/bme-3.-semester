package labor;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BusStopFilter {

    private double lat;
    private double lon;

    Document document;

    public BusStopFilter(Document doc, double lat, double lon) {
        document = doc;
        this.lat = lat;
        this.lon = lon;
        iterate(doc.getRootElement().getChildren());
    }
    

    private void iterate(List<Element> elements) {
        List<Element> toRemove = new ArrayList<>();
        for (Element e: elements) {
            if (e.getName().equals("node")){
                if(containsBusStop(e.getChildren())) {
                    double dist = dist1(lat, lon, Double.parseDouble(e.getAttributeValue("lat")), Double.parseDouble(e.getAttributeValue("lon")));
                    e.addContent(new Element("tag").setAttributes(Arrays.asList(
                            new Attribute("k", "distance"),
                            new Attribute("v", String.valueOf(dist))
                    )));
                } else {
                    toRemove.add(e);
                    continue;
                }
            }
            iterate(e.getChildren());
        }
        toRemove.forEach(Element::detach);
    }
    
    private boolean containsBusStop(List<Element> elements) {
        Element element = elements.stream().filter(e -> {
            if (e.getName().equals("tag")) {
                return e.getAttribute("v").getValue().equals("bus_stop");
            }
            return false;
        }).findFirst().orElse(null);

        return element != null;
    }

    public Document getDocument(){
        return document;
    }

    private double dist1(double lat1, double lon1, double lat2, double lon2) {
        double R = 6371000;
        double phi1 = Math.toRadians(lat1);
        double phi2 = Math.toRadians(lat2);
        double dphi = phi2 - phi1;
        double dl = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dphi / 2) * Math.sin(dphi / 2) + Math.cos(phi1) * Math.cos(phi2) * Math.sin(dl / 2) * Math.sin(dl / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;
        return d;
    }
}
