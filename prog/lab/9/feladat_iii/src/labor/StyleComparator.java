package labor;

import java.util.Comparator;

public class StyleComparator implements Comparator<Beer> {
    @Override
    public int compare(Beer one, Beer two) {
        return one.getStyle().compareTo(two.getStyle());
    }
}
