package labor;

import javax.json.*;
import java.io.*;
import java.util.ArrayList;

public class Load extends Command {
    public Load(CollectionWrapper beers) {
        super("load", beers);
    }

    @Override
    public void run(String[] args) {
        if (args.length < 1) {
            System.out.println("usage: load <file>");
            return;
        }

        try {
            InputStream is = new FileInputStream(args[0]);
            JsonReader jsonReader = Json.createReader(is);
            JsonArray jsonArray = jsonReader.readArray();
            jsonReader.close();
            is.close();
            
            beers.beers = new ArrayList<>(jsonArray.size());
            
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jsonObject = jsonArray.getJsonObject(i);
                beers.beers.add(new Beer(jsonObject.getString("name"), jsonObject.getString("style"), jsonObject.getJsonNumber("strength").doubleValue()));
            }
        } catch (Exception e) {
            System.out.println("error loading file");
        }
    }
}
