package labor;

public abstract class Command {
    private final String name;
    protected CollectionWrapper beers;

    public Command(String name, CollectionWrapper beers) {
        this.name = name;
        this.beers = beers;
    }

    public String getName() {
        return name;
    }

    public void run(String[] args) {}

    public static String[] cutArgs(String[] args) {
        String[] ret = new String[args.length-1];
        if (args.length >= 1) System.arraycopy(args, 1, ret, 0, args.length - 1);
        return ret;
    }

}

