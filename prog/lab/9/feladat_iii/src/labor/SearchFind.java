package labor;

public class SearchFind extends Command {

    private final boolean fullMatch;

    public SearchFind(String name, CollectionWrapper beers, boolean full) {
        super(name, beers);
        fullMatch = full;
    }


    @Override
    public void run(String[] args) {
        if (args.length < 1) {
            System.out.println("usage: "+getName()+" [attribute] <name>");
            return;
        }
        BeerMatcher matcher = new NameMatcher(fullMatch);
        String search = args[0];

        if (args.length >= 2) {
            search = args[1];
            switch (args[0]) {
                case "name":
                    matcher = new NameMatcher(fullMatch);
                    break;
                case "style":
                    matcher = new StyleMatcher(fullMatch);
                    break;
                case "strength":
                    matcher = new StrengthMatcher(fullMatch);
                    break;
                case "weaker":
                    if (fullMatch) {
                        System.out.println("weaker is not applicable for a lousy match. use \"find weaker\" or \"match strength\"");
                        return;
                    }
                    matcher = new WeakerMatcher();
                    break;
                default:
                    System.out.println("\""+args[0]+"\" is not an attribute");
                    System.out.println("usage: "+getName()+" [attribute] <name>");
                    return;
            }
        }

        final String finalSearch = search;
        final BeerMatcher finalMatcher = matcher;
        beers.beers.forEach((i) -> {
            if (finalMatcher.match(i, finalSearch)) {
                System.out.println(i.toString());
            }
        });

    }
}
