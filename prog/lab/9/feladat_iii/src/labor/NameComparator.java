package labor;

import java.util.Comparator;

public class NameComparator implements Comparator<Beer> {
    @Override
    public int compare(Beer one, Beer two) {
        return one.getName().compareTo(two.getName());
    }
}
