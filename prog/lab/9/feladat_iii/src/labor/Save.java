package labor;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import java.io.*;

public class Save extends Command {
    public Save(CollectionWrapper beers) {
        super("save", beers);
    }

    @Override
    public void run(String[] args) {
        if (args.length < 1) {
            System.out.println("usage: save <filename>");
            return;
        }

        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        for(Beer b: beers.beers) {
            JsonObjectBuilder beerBuilder = Json.createObjectBuilder();
            beerBuilder.add("name", b.name);
            beerBuilder.add("strength", b.strength);
            beerBuilder.add("style", b.style);
            jsonArrayBuilder.add(beerBuilder);
        }

        try {
            FileWriter f = new FileWriter(args[0]);
            f.write(jsonArrayBuilder.build().toString());
            f.close();
        } catch (Exception e) {
            System.out.println("error writing file");
        }
    }
}
