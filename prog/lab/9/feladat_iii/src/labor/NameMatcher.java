package labor;

public class NameMatcher implements BeerMatcher {
    boolean full;
    public NameMatcher(boolean full) {
        this.full = full;
    }

    @Override
    public boolean match(Beer b, String o) {
        if (full)
            return b.getName().equals(o);
        else
            return b.getName().contains(o);
    }
}
