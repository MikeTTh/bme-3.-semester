package labor;

import java.util.Comparator;

public class StrengthComparator implements Comparator<Beer> {
    @Override
    public int compare(Beer one, Beer two) {
        return (int) (one.getStrength() - two.getStrength());
    }
}
