package labor;

import java.io.Serializable;

public class Beer implements Serializable {
    String name;
    String style;
    double strength;

    public Beer(String name, String jelleg, double strength) {
        this.name = name;
        this.style = jelleg;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public double getStrength() {
        return strength;
    }

    public String getStyle() {
        return style;
    }

    @Override
    public String toString() {
        return "labor.Beer{" +
                "name='" + name + '\'' +
                ", jelleg='" + style + '\'' +
                ", strength=" + strength +
                '}';
    }
}
