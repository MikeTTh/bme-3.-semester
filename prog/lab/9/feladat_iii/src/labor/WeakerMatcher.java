package labor;

public class WeakerMatcher implements BeerMatcher {
    @Override
    public boolean match(Beer b, String o) {
        try {
            double strength = Double.parseDouble(o);
            return b.getStrength() <= strength;
        } catch (Exception e) {
            return false;
        }
    }
}
