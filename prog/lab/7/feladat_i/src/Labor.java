import javax.swing.*;
import static javax.swing.GroupLayout.Alignment.*;
import java.awt.event.*;
import java.beans.*;



public class Labor
        implements Runnable, ActionListener, PropertyChangeListener {

    /* a k�t updater, ha �ppen futnak */
    Updater u1;
    Updater u2;

    public Labor() {
        /* kezdetben nincs munka */
        u1 = null;
        u2 = null;
    }

    /** a gombokt�l j�v� esem�nyek kezel�se */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button1) {
            if (u1 != null) {
                u1.cancel(true);
            }
            u1 = new Updater();
            u1.addPropertyChangeListener(this);
            u1.execute();
        } else {
            if (u2 != null) {
                u2.cancel(true);
            }
            u2 = new Updater();
            u2.addPropertyChangeListener(this);
            u2.execute();
        }
    }

    /** az updater-ekt�l j�v� esem�nyek kezel�se */
    public void propertyChange(PropertyChangeEvent evt) {
        JProgressBar p = progress1;
        JTextField t = field1;
        if (evt.getSource() == u2) {
            p = progress2;
            t = field2;
        }

        switch (evt.getPropertyName()) {
            case "progress":
                p.setValue((Integer) evt.getNewValue());
                break;
            case "state":
                if (SwingWorker.StateValue.DONE.equals(evt.getNewValue())) {
                    Updater u = (Updater) evt.getSource();
                    try {
                        t.setText(String.valueOf(u.get()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        }

    }

    /* az esem�nykezel�s sor�n el�rend� elemek */
    JTextField field1;
    JProgressBar progress1;
    JTextField field2;
    JProgressBar progress2;
    JButton button1;
    JButton button2;

    /** a frame-et fel�p�t� k�d */
    public void run() {
        /* frame l�trehoz�sa */
        JFrame f = new JFrame();
        /* layout hozz�rendel�se. M�g nincs meg a kioszt�s!!! */
        GroupLayout layout = new GroupLayout(f.getContentPane());
        f.getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        /* a fels� sor elemei */
        JLabel label1 = new JLabel("First row");
        field1 = new JTextField(10);
        button1 = new JButton("Update");
        button1.addActionListener(this);
        progress1 = new JProgressBar(0,99);


        /* az als� sor elemei */
        JLabel label2 = new JLabel("Second row");
        field2 = new JTextField(10);
        button2 = new JButton("Update");
        button2.addActionListener(this);
        progress2 = new JProgressBar(0,99);

        layout.setHorizontalGroup(
            layout.createSequentialGroup()
                .addGroup(
                    layout.createParallelGroup(LEADING)
                        .addComponent(label1)
                        .addComponent(label2)
                )
                .addGroup(
                    layout.createParallelGroup(LEADING)
                        .addComponent(field1)
                        .addComponent(field2)
                )
                .addGroup(
                    layout.createParallelGroup(LEADING)
                        .addComponent(button1)
                        .addComponent(button2)
                )
                .addGroup(
                    layout.createParallelGroup(LEADING)
                        .addComponent(progress1)
                        .addComponent(progress2)
                )
        );

        layout.setVerticalGroup(
            layout.createSequentialGroup()
                .addGroup(
                    layout.createParallelGroup(CENTER)
                        .addComponent(label1)
                        .addComponent(field1)
                        .addComponent(button1)
                        .addComponent(progress1)
                )
                .addGroup(
                    layout.createParallelGroup(CENTER)
                        .addComponent(label2)
                        .addComponent(field2)
                        .addComponent(button2)
                        .addComponent(progress2)
                )
        );

        /* a textfield-ek nem ny�lnak f�gg�legesen */
        layout.linkSize(SwingConstants.VERTICAL, field1, field2);

        /* a frame be�ll�t�sa */
        f.pack();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.setTitle("Swing Labor");
        f.setVisible(true);
    }

    static public void main(String[] args) {
        SwingUtilities.invokeLater(new Labor());
    }
}

