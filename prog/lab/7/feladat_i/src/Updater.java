import javax.swing.*;


public class Updater extends SwingWorker<Double, Object> {

    public Double doInBackground() {
        Calculator c = new Calculator();
        try {
            while (!isCancelled()){
                int progress = c.work();
                setProgress(progress);

                if (progress >= 100)
                    break;
            }
        } catch (InterruptedException e) {}

        return c.get();
    }
}
