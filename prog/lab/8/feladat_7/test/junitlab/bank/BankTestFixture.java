package junitlab.bank;

import junitlab.bank.impl.FirstNationalBank;
import junitlab.bank.impl.GreatSavingsBank;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BankTestFixture {
    Bank bank;
    String acc1;
    String acc2;


    @Before
    public void setUp() throws AccountNotExistsException {
        bank = new GreatSavingsBank();
        acc1 = bank.openAccount();
        acc2 = bank.openAccount();
        bank.deposit(acc1, 1500);
        bank.deposit(acc2, 12000);

    }

    @Test
    public void testTransfer() throws AccountNotExistsException, NotEnoughFundsException {
        bank.transfer(acc2, acc1, 3456);
        Assert.assertEquals(bank.getBalance(acc1), 4956);
        Assert.assertEquals(bank.getBalance(acc2), 8544);
    }

    @Test(expected = NotEnoughFundsException.class)
    public void testTransferWithoutEnoughFunds() throws AccountNotExistsException, NotEnoughFundsException {
        bank.transfer(acc1, acc2, 3456);
    }
}
