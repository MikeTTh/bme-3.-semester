package junitlab.bank;

import junitlab.bank.impl.FirstNationalBank;
import junitlab.bank.impl.GreatSavingsBank;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;

@RunWith(Parameterized.class)
public class BankParamTest {
    int amount;
    int rounded;

    public BankParamTest(int amount, int rounded){
        this.amount = amount;
        this.rounded = rounded;
    }

    @Parameterized.Parameters
    public static List<Integer[]> getPairs() {
        List<Integer[]> ret = new ArrayList<>();
        ret.add(new Integer[] {1100, 1100});
        ret.add(new Integer[] {1101, 1100});
        ret.add(new Integer[] {1149, 1100});
        ret.add(new Integer[] {1150, 1200});
        ret.add(new Integer[] {1151, 1200});
        ret.add(new Integer[] {1199, 1200});
        ret.add(new Integer[] {1200, 1200});
        return ret;
    }

    @Test
    public void testWithdrawRounding() throws AccountNotExistsException, NotEnoughFundsException {
        Bank b = new GreatSavingsBank();
        String acc = b.openAccount();
        b.deposit(acc, 10000);
        long before = b.getBalance(acc);
        b.withdraw(acc, amount);
        long after = b.getBalance(acc);
        Assert.assertEquals(before-rounded, after);
    }
}
