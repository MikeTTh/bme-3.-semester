package junitlab.bank;

import junitlab.bank.impl.FirstNationalBank;
import junitlab.bank.impl.GreatSavingsBank;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BankTest {

    Bank bank;

    @Before
    public void setUp(){
        bank = new GreatSavingsBank();
    }

    @Test
    public void testOpenAccount() throws AccountNotExistsException {
        String account = bank.openAccount();
        long balance = bank.getBalance(account);
        Assert.assertEquals(balance, 0L);
    }

    @Test
    public void testUniqueAccount() {
        String acc1 = bank.openAccount();
        String acc2 = bank.openAccount();
        Assert.assertNotEquals(acc1, acc2);
    }

    @Test(expected = AccountNotExistsException.class)
    public void testInvalidAccount() throws AccountNotExistsException {
        long bal = bank.getBalance("nemletezoszam");
    }

    @Test
    public void testDeposit() throws AccountNotExistsException {
        String acc = bank.openAccount();
        bank.deposit(acc, 2000);
        long bal = bank.getBalance(acc);
        Assert.assertEquals(bal, 2000L);
    }
}