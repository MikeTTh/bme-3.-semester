package junitlab.bank;

import junitlab.bank.impl.GreatSavingsBank;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class NewBankTest {

    NewBank newBank;

    @Mock
    GreatSavingsBank bank;

    @Before
    public void setUp() {
        bank = Mockito.mock(GreatSavingsBank.class);
        newBank = new NewBank(bank);
    }

    @Test
    public void testOpenAccountAndDeposit() throws AccountNotExistsException {
        String accname = "cucc";

        Mockito.when(bank.openAccount()).thenReturn(accname);
        String acc = newBank.openAccountAndDeposit(1000);

        Assert.assertEquals(acc, accname);

        Mockito.verify(bank, Mockito.times(1)).openAccount();
        Mockito.verify(bank, Mockito.times(1)).deposit(accname, 1000);
    }

    @After
    public void tearDown(){
        Mockito.verifyNoMoreInteractions(bank);
    }

    /*
    @Test
    public void testTransferAllAndCloseSourceAccount() throws AccountNotExistsException, NotEnoughFundsException {
        newBank.transferAllAndCloseSourceAccount("acc1", "acc2");
    }
    */
}