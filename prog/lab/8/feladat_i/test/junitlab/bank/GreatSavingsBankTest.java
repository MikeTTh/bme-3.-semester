package junitlab.bank;

import junitlab.bank.impl.GreatSavingsBank;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class GreatSavingsBankTest {
    GreatSavingsBank bank;

    @Before
    public void setUp(){
        bank = new GreatSavingsBank();
    }

    @Test
    public void accountHoldsMoney() throws AccountNotExistsException, NotEnoughFundsException {
        String acc = bank.openAccount();
        bank.deposit(acc, 1000);
        long money = bank.getBalance(acc);
        Assert.assertEquals(money, 1000);

        bank.withdraw(acc, 1000);
        money = bank.getBalance(acc);
        Assert.assertEquals(money, 0);
    }

    @Test
    public void closeOpenedAccount() throws AccountNotExistsException {
        String acc = bank.openAccount();
        boolean empty = bank.closeAccount(acc);
        Assert.assertTrue(empty);

        acc = bank.openAccount();
        bank.deposit(acc, 1000);
        empty = bank.closeAccount(acc);
        Assert.assertFalse(empty);
    }

    @Test(expected = AccountNotExistsException.class)
    public void closeUnopenedAccount() throws AccountNotExistsException {
        bank.closeAccount("asd");
    }

    @Test(expected = NotEnoughFundsException.class)
    public void testNotEoughFunds() throws NotEnoughFundsException, AccountNotExistsException {
        String acc = bank.openAccount();
        bank.withdraw(acc, 1000);
    }

    @Test
    public void testTransfer() throws AccountNotExistsException, NotEnoughFundsException {
        String acc1 = bank.openAccount();
        String acc2 = bank.openAccount();
        bank.deposit(acc1, 5000);
        bank.deposit(acc2, 5000);

        bank.transfer(acc1, acc2, 1000);

        Assert.assertEquals(bank.getBalance(acc1), 4000);
        Assert.assertEquals(bank.getBalance(acc2), 6000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotEnoughFundsWhileTransfer() throws AccountNotExistsException, NotEnoughFundsException {
        String acc1 = bank.openAccount();
        String acc2 = bank.openAccount();
        bank.deposit(acc1, 5000);
        bank.deposit(acc2, 5000);

        bank.transfer(acc1, acc2, -5000);
    }

}
