import java.util.Random;

public class Application {

    public static void main(String[] args) {
        Fifo f = new Fifo();

        Random r = new Random();


        (new Producer("prod", f, 1000)).start();
        (new Consumer(f, "cons", r.nextInt(10000))).start();
    }
}
