import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        Fifo f = new Fifo();


        ScheduledThreadPoolExecutor prods = new ScheduledThreadPoolExecutor(10);
        ScheduledThreadPoolExecutor cons = new ScheduledThreadPoolExecutor(10);

        for (int i = 0; i < 15; i++) {
            Producer p = new Producer("prod"+i, f, 5000);
            prods.schedule(p, 1, TimeUnit.MILLISECONDS);

            Consumer c = new Consumer(f, "cons"+i, 5000);
            cons.schedule(c, 1, TimeUnit.MILLISECONDS);

            Thread.sleep(300);
        }

    }
}
