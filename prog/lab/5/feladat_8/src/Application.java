import java.util.Random;

public class Application {

    public static void main(String[] args) {
        Fifo f = new Fifo();

        Random r = new Random();

        for (int i = 0; i < 3; i++) {
            (new Thread(new Producer("prod"+i, f, r.nextInt(10000)))).start();
        }


        for (int i = 0; i < 4; i++) {
            (new Thread(new Consumer(f, "cons"+i, r.nextInt(10000)))).start();
        }
    }
}
