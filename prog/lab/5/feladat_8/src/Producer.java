public class Producer implements Runnable {
    int i = 0;
    String name = "";
    Fifo f;
    int n;

    public Producer(String name, Fifo f, int n) {
        this.name = name;
        this.f = f;
        this.n = n;
    }

    @Override
    public void run() {
        try {
            go();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void go() throws InterruptedException {
        while (true) {
            String s = String.format("%s %d %d", name, i, System.currentTimeMillis()%100000);
            f.put(s);
            System.out.printf("produced %s\n", s);
            i++;
            try {
                Thread.sleep(n);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
