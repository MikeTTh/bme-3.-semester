import java.util.LinkedList;

public class Fifo {
    private LinkedList<String> list = new LinkedList<>();
    public synchronized void put(String in) throws InterruptedException {
        while (list.size() >= 10) {
            this.wait();
        }

        this.notify();
        System.out.printf("put %s\n", Thread.currentThread().getName());
        list.push(in);
    }

    public synchronized String get() throws InterruptedException {
        while (list.size() < 1) {
            this.wait();
        }

        this.notify();
        System.out.printf("put %s\n", Thread.currentThread().getName());
        return list.pop();
    }
}
