public class Consumer implements Runnable {
    Fifo f;
    String s;
    int n;

    public Consumer(Fifo f, String s, int n) {
        this.f = f;
        this.s = s;
        this.n = n;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.printf("consumed %s %s %d\n", s, f.get(), System.currentTimeMillis()%100000);
                Thread.sleep(n);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
