package szoftlab3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColorChanger implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        Draw.setColor(JColorChooser.showDialog(null, "Set color", Draw.getColor()));
    }
}
