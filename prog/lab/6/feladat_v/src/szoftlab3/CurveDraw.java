package szoftlab3;

import java.awt.*;

public class CurveDraw extends Draw {

    Integer lastx = null;
    Integer lasty = null;

    @Override
    protected void makeDraw(Graphics g) {
        if (lastx == null)
            lastx = startx;
        if (lasty == null)
            lasty = starty;

        g.drawLine(lastx, lasty, endx, endy);

        lastx = endx;
        lasty = endy;
    }
}
