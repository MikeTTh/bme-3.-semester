package szoftlab3;

import java.awt.*;

public class CircleDraw extends Draw {
    public void makeDraw(Graphics g) {
        int r = (int) Math.sqrt( Math.pow(dif(startx, endx),2)+Math.pow(dif(starty, endy),2));
        int leftx = startx-r;
        int upy = starty-r;
        g.drawOval(leftx, upy, 2*r, 2*r);
    }
}
