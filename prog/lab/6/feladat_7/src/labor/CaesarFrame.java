package labor;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

public class CaesarFrame extends JFrame {
    private static JComboBox<Character> comboBox = new JComboBox<>(alphabet());
    private static JTextField input = new JTextField(20);
    private static JButton button = new JButton("Code!");
    private static JPanel up = new JPanel();

    private static JLabel label = new JLabel("Output:");
    private static JTextField output = new JTextField(20);
    private static JPanel down = new JPanel();

    private static GridLayout grid = new GridLayout(0,1);

    private Component lastFocus;

    private static Character[] alphabet() {
        Character[] out = new Character['Z'-'A'+1];
        char cur = 'A';
        for (int i = 0; i < out.length; i++) {
            out[i] = cur;
            cur++;
        }
        return out;
    }

    public CaesarFrame() {
        super("SwingLab");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 110);

        up.add(comboBox);
        up.add(input);
        input.addFocusListener(new focusListener());
        up.add(button);
        button.addActionListener(new OkButtonActionListener());
        input.getDocument().addDocumentListener(new InputDocumentListener());
        // input.addKeyListener(new InputFieldKeyEvent());

        down.add(label);
        down.add(output);
        output.addFocusListener(new focusListener());

        add(up);
        add(down);
        setLayout(grid);
    }

    private class focusListener implements FocusListener {
        @Override
        public void focusGained(FocusEvent e) {
            lastFocus = e.getComponent();
        }

        @Override
        public void focusLost(FocusEvent e) {}
    }

    public class OkButtonActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (lastFocus == input) {
                output.setText(Caesar.caesarCode(input.getText(), (Character) comboBox.getSelectedItem()));
            } else {
                input.setText(Caesar.caesarDecode(output.getText(), (Character) comboBox.getSelectedItem()));
            }
        }
    }

    /*
    private static class InputFieldKeyEvent extends KeyAdapter {
        @Override
        public void keyTyped(KeyEvent e) {
            output.setText(Caesar.caesarCode(input.getText(), (char)comboBox.getSelectedItem()));
        }
    }
    */

    private class InputDocumentListener implements DocumentListener {

        private void update() {
            if (lastFocus == input) {
                output.setText(Caesar.caesarCode(input.getText(), (char) comboBox.getSelectedItem()));
            }
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            update();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            update();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            update();
        }
    }

}
