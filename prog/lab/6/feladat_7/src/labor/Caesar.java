package labor;

public class Caesar {
    private static String caesar(String input, char offset) {
        StringBuilder out = new StringBuilder();

        for (char c : input.toCharArray()) {
            if (c == ' '){
                out.append(c);
                continue;
            }

            c += offset;

            if (c > 'Z') {
                c = (char) ('A' + (c-'Z'));
            }

            if (c < 'A'){
                c = (char) ('Z'-('A'-c));
            }

            out.append(c);
        }

        return out.toString();
    }

    public static String caesarCode(String input, char offset) {
        input = input.toUpperCase();
        offset = Character.toUpperCase(offset);
        offset = (char) (offset - 'A');

        return caesar(input, offset);
    }

    public static String caesarDecode(String input, char offset) {
        input = input.toUpperCase();
        offset = Character.toUpperCase(offset);
        offset = (char) ('A' - offset);

        return caesar(input, offset);
    }
}
