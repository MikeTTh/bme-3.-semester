package labor;

public class Caesar {
    public static String caesarCode(String input, char offset) {
        input = input.toUpperCase();
        offset = Character.toUpperCase(offset);
        offset = (char) (offset - 'A');

        StringBuilder out = new StringBuilder();

        for (char c : input.toCharArray()) {
            if (c == ' '){
                out.append(c);
                continue;
            }

            c += offset;
            if (c > 'Z') {
                c = (char) ('A' + (c-'Z'));
            }
            out.append(c);
        }

        return out.toString();
    }
}
