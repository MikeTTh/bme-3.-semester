package labor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CaesarFrame extends JFrame {
    private static JComboBox<Character> comboBox = new JComboBox<>(alphabet());
    private static JTextField input = new JTextField(20);
    private static JButton button = new JButton("Code!");
    private static JPanel up = new JPanel();

    private static JLabel label = new JLabel("Output:");
    private static JTextField output = new JTextField(20);
    private static JPanel down = new JPanel();

    private static GridLayout grid = new GridLayout(0,1);

    private static Character[] alphabet() {
        Character[] out = new Character['Z'-'A'+1];
        char cur = 'A';
        for (int i = 0; i < out.length; i++) {
            out[i] = cur;
            cur++;
        }
        return out;
    }

    public CaesarFrame() {
        super("SwingLab");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 110);

        up.add(comboBox);
        up.add(input);
        up.add(button);
        button.addActionListener(new OkButtonActionListener());

        down.add(label);
        down.add(output);
        output.setEditable(false);

        add(up);
        add(down);
        setLayout(grid);
    }

    public static class OkButtonActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            output.setText(Caesar.caesarCode(input.getText(), (char)comboBox.getSelectedItem()));
        }
    }
}
