import java.io.File;

public class Exit implements Command {
    public String name() {
        return "exit";
    }
    public String help() {
        return "exit: closes shell";
    }
    public File execute(File wd, String[] cmd) {
        System.exit(0);
        return wd;
    }
}
