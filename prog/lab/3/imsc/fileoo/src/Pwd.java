import java.io.File;
import java.io.IOException;

public class Pwd implements Command {
    public String name() {
        return "pwd";
    }
    public String help() {
        return "pwd: prints out current directory";
    }
    public File execute(File wd, String[] args) {
        try {
            System.out.println(wd.getCanonicalPath());
        } catch (IOException e) {
            System.out.println(wd.getAbsolutePath());
        }
        return wd;
    }
}
