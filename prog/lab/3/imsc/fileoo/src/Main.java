import java.io.File;
import java.util.Scanner;

public class Main {

    private static final Command[] commands = { new Exit(), new Pwd(), new Cd(), new Ls(), new Cat(), new Mv(), new Wc() };

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        File f = new File(System.getProperty("user.dir"));

        System.out.print(f.getName());
        System.out.print(" > ");
        while (scan.hasNextLine()) {
            boolean ran = false;
            String[] cmd = scan.nextLine().split(" ");
            for (Command c : commands) {
                if (c.name().equals(cmd[0])) {
                    f = c.execute(f, cutFirst(cmd));
                    ran = true;
                }
            }
            if (cmd[0].equals("help")) {
                for (Command c : commands) {
                    System.out.println(c.help());
                }
                ran = true;
            }

            if (!ran) {
                System.out.printf("command \"%s\" not found\n", cmd[0]);
            }

            System.out.print(f.getName());
            System.out.print(" > ");
        }
    }

    private static String[] cutFirst(String[] strings) {
        String[] ret = new String[strings.length-1];
        if (strings.length - 1 >= 0)
            System.arraycopy(strings, 1, ret, 0, strings.length - 1);
        return ret;
    }
}
