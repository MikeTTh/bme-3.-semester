import java.io.File;
import java.util.Objects;

public class Ls implements Command {
    public String name() {
        return "ls";
    }
    public String help() {
        return "ls [-l]: lists files/dirs in current directory\n\t-l: more information";
    }
    public File execute(File f, String[] args) {
        if (f == null || !f.exists()) {
            System.out.println("Current directory doesn't exist");
            return f;
        }

        boolean list = false;
        if (args.length >= 1 && args[0].equals("-l")) {
            list = true;
        }

        int longest = 0;
        for(File fi : Objects.requireNonNull(f.listFiles())) {
            int l = fi.getName().length();
            if (l > longest) {
                longest = l;
            }
        }


        for(File fi : Objects.requireNonNull(f.listFiles())) {
            String name = fi.getName();
            int numOfSpaces = longest-name.length()+1;

            System.out.print(name);
            System.out.printf("%1$"+numOfSpaces+"s\t", "");
            if (list) {

                System.out.print(fileSizeToString(fi));
                System.out.print("\t"+(fi.isDirectory()?"d":"f"));
            }
            System.out.println();
        }
        return f;
    }
    private String fileSizeToString(File f) {
        double length = f.length();
        String[] sizes = {"", "K", "M", "G", "T"};
        int i = 0;
        for(String s : sizes) {
            if (length >= 1024) {
                length /= 1024;
                i++;
            } else {
                break;
            }
        }
        if (i == 0){
            return String.format("%dB  ", f.length());
        }
        return String.format("%.2f%sB", length, sizes[i]);
    }
}
