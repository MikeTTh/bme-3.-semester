import java.io.*;
import java.util.StringTokenizer;

public class Wc implements Command {
    public String name() {
        return "wc";
    }
    public String help() {
        return "wc <file>: counts lines, words, letters in file";
    }
    public File execute(File f, String[] args) {
        if (args.length < 1) {
            System.out.println("usage: wc <file>");
            return f;
        }

        try {
            int lines = 0, words = 0, letters = 0;
            BufferedReader read = new BufferedReader(new FileReader(new File(f, args[0])));
            String line;
            while ((line = read.readLine()) != null){
                StringTokenizer tok = new StringTokenizer(line);
                lines++;
                words += tok.countTokens();
                letters += line.length()+1;
            }
            letters--;
            System.out.printf("%d %d %d\n", lines, words, letters);
        } catch (FileNotFoundException e) {
            System.out.println("Couldn't open file");
            return f;
        } catch (IOException e) {
            System.out.println("Couldn't read file");
        }

        return f;
    }
}
