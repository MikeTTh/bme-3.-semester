import java.io.*;
import java.util.Scanner;

public class Cat implements Command {
    public String name() {
        return "cat";
    }
    public String help() {
        return "cat <file>: prints file to stdout";
    }
    public File execute(File f, String[] args) {
        if (args.length < 1) {
            System.out.println("usage: "+name()+" <file>");
            return f;
        }
        File nf = new File(f, args[0]);
        if (nf.exists()) {
            try {
                BufferedReader read = new BufferedReader(new FileReader(nf));
                String line;
                while ((line = read.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
                return f;
            } catch (IOException e) {
                System.out.println("Couldn't read file");
                return f;
            }
        }
        return f;
    }
}
