import java.io.*;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Main {

    public static void main(String[] args) {
        if (args == null || args.length < 1) {
            System.out.println("usage: grep <pattern>");
            System.exit(1);
        }
        String input = null;
        String output = null;
        String pattern = "";
        boolean gi = false;
        boolean go = false;

        for (int i = 0; i < args.length; i++) {
            if ((i+1 < args.length) && args[i].equals("-i")) {
                i++;  input = args[i];
            } else if ((i+1 < args.length) && args[i].equals("-o")) {
                i++; output = args[i];
            } else if ((i+1 < args.length) && args[i].equals("-p")) {
                i++; pattern = args[i];
            } else if (args[i].equals("-gi")) {
                gi = true;
            } else if (args[i].equals("-go")) {
                go = true;
            }
        }

        if (args.length == 1)
            pattern = args[0];

        InputStream in = System.in;
        if (input != null) {
            try {
                in = new FileInputStream(input);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        if (gi) {
            try {
                in = new GZIPInputStream(in);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        OutputStream out = System.out;
        if (output != null) {
            try {
                out = new FileOutputStream(output);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        if (go) {
            try {
                out = new GZIPOutputStream(out);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        Scanner scan = new Scanner(in);
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.matches(".*"+pattern+".*")) {
                line += "\n";
                try {
                    out.write(line.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
