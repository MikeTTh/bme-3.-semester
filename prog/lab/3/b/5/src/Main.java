import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("usage: grep <pattern>");
            System.exit(1);
        }

        Scanner scan = new Scanner(System.in);
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.matches(".*"+args[0]+".*")) {
                System.out.println(line);
            }
        }
    }
}
