import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        if (args == null || args.length < 1) {
            System.out.println("usage: grep <pattern>");
            System.exit(1);
        }
        String input = null;
        String output = null;
        String pattern = "";

        for (int i = 0; i < args.length; i++) {
            if ((i+1 < args.length) && args[i].equals("-i")) {
                i++;  input = args[i];
            } else if ((i+1 < args.length) && args[i].equals("-o")) {
                i++; output = args[i];
            } else if ((i+1 < args.length) && args[i].equals("-p")) {
                i++; pattern = args[i];
            }
        }

        if (args.length == 1)
            pattern = args[0];

        InputStream in = System.in;
        if (input != null) {
            try {
                in = new FileInputStream(new File(input));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        OutputStream out = System.out;
        if (output != null) {
            try {
                out = new FileOutputStream(new File(output));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

        Scanner scan = new Scanner(in);
        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.matches(".*"+pattern+".*")) {
                line += "\n";
                try {
                    out.write(line.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }
    }
}
