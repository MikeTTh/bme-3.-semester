import java.io.File;

public class Mv implements Command {
    public String name() {
        return "mv";
    }
    public File execute(File f, String[] args) {
        if (args.length < 2) {
            System.out.println("usage: <what> <where>");
            return f;
        }

        File fi = new File(f, args[0]);
        File dest = new File(f, args[1]);
        try {
            if (!fi.renameTo(dest)) {
                System.out.println("couldn't move file");
            }
        } catch (Exception e) {
            System.out.println("couldn't move file");
        }

        return f;
    }
}
