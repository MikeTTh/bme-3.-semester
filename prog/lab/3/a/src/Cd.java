import java.io.File;

public class Cd implements Command {
    public String name() {
        return "cd";
    }

    public File execute(File f, String[] args) {
        if (args.length < 1) {
            System.out.println("usage: "+name()+" <directory>");
            return f;
        }
        File nf;
        if (args[0].equals("..")) {
            nf = f.getParentFile();
        } else {
            nf = new File(f, args[0]);
        }
        if (nf.exists() && nf.isDirectory()) {
            return nf;
        }
        System.out.println("Directory does not exist");
        return f;
    }
}
