import sample.calc.Calculator;

public class Main {

    public static void main(String[] args) {
        int sum = 0;
        for (int i = 0; i < args.length; i++) {
            int input = Integer.parseInt(args[i]);
            Calculator calc = new Calculator();
            sum = calc.add(sum, input);
        }
        System.out.println(sum);
    }
}
