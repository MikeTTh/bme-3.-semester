-- general settings
-- ================
-- do not echo
set echo off
-- do not print substitution before/after text
set verify off
-- set date format
alter session set NLS_DATE_FORMAT='YYYY-MM-DD';
-- allow PL/SQL output
set serveroutput on
-- disable feedback, eg. anonymous block completed
set feedback off

-- táblaeldobások ide
drop table FOGLALAS;
drop table UTASOK;
drop table ULESEK;
drop sequence utasid_seq;
drop sequence ulesid_seq;
drop sequence foglalas_seq;

prompt <tasks>

prompt <task n="2">
prompt <![CDATA[

CREATE SEQUENCE utasid_seq;

CREATE TABLE utasok (
    id Number default utasid_seq.nextval NOT NULL,
    igazolvany VARCHAR(20) NOT NULL,
    nev VARCHAR(50) NOT NULL,
    szuletes DATE check ( szuletes > date'1910-01-01' ),
    lakcim LONG NOT NULL,
    telefonszam Number(12) check ( telefonszam>=10000 ),
    klubtagsag nvarchar2(8),
    akciok Integer default 1 check ( akciok > 0 ),
    constraint utasok_pk primary key (id),
    constraint utasok_igazolvany unique (igazolvany),
    constraint klubtagsag_chk check ( klubtagsag in ('SILVER', 'GOLD', 'PLATINUM'))
);

CREATE SEQUENCE ulesid_seq;

CREATE TABLE ulesek (
    id Number default ulesid_seq.nextval NOT NULL,
    repulo VARCHAR(20) NOT NULL,
    oszlop CHAR(1) check ( REGEXP_LIKE(oszlop, '[A-M]') ),
    sor Integer check ( sor >=1 and sor <= 24 ),
    business Integer default 0 check ( business = 0 or business = 1),
    constraint ulesek_pk primary key (id)
);

CREATE SEQUENCE foglalas_seq;

CREATE TABLE foglalas(
    id Number default foglalas_seq.nextval NOT NULL,
    utasid Number NOT NULL,
    ulesid Number NOT NULL,
    jarat VARCHAR(50) NOT NULL,
    utazas DATE NOT NULL,
    foglalas DATE,
    constraint foglalas_pk primary key (id),
    constraint utasid_key foreign key (utasid) references utasok(id),
    constraint ulesid_key foreign key (ulesid) references ulesek(id),
    constraint foglalas_regebbi_mint_utazas check ( foglalas < utazas or foglalas IS NULL )
);

prompt ]]>
prompt </task>

prompt </tasks>
