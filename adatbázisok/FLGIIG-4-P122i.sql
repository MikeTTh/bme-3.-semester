set echo off
set verify off
alter session set NLS_DATE_FORMAT='YYYY-MM-DD';
set serveroutput on
set feedback off

prompt <tasks>

prompt <task n="3.1">
prompt <![CDATA[

insert into PASSENGER
values
       (DEFAULT, 'Nemes Csilla', date'1999-09-08', 'Budapest, Hegyalja út 12', 52377211, 'GOLD', 5020);

prompt ]]>
prompt </task>


prompt <task n="3.2">
prompt <![CDATA[

update PASSENGER
set P_NAME = 'Seres János', TIER = 'SILVER', POINTS = 400
where P_ID = 28;

prompt ]]>
prompt </task>


prompt <task n="3.3">
prompt <![CDATA[

update PASSENGER
set POINTS = POINTS*1.25
where DATEOFBIRTH > ADD_MONTHS(CURRENT_DATE, -8*12);

prompt ]]>
prompt </task>


prompt <task n="3.4">
prompt <![CDATA[

update PASSENGER
set POINTS = POINTS*1.15
where P_ID in (
    select P_ID from BOOKING
    where BOOKING_DATE <= date'2010-01-01'
);

prompt ]]>
prompt </task>


prompt <task n="3.5">
prompt <![CDATA[

delete BOOKING
where BOOKING_DATE <= date'2008-12-31';

prompt ]]>
prompt </task>


prompt <task n="3.6">
prompt <![CDATA[

insert into BOOKING (BOOKING_ID, SEAT_ID, P_ID, FLIGHT_NUMBER, TRAVEL_DATE)
values (
    DEFAULT,
    (select SEAT_ID from SEAT where SEAT_COL = 'A' and SEAT_ROW = 22),
    (select P_ID from PASSENGER where P_NAME = 'Simon Antal'),
    2221,
    date'2020-11-01'
);

prompt ]]>
prompt </task>



prompt </tasks>