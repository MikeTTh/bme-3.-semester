set echo off
set verify off
alter session set NLS_DATE_FORMAT='YYYY-MM-DD';
set serveroutput on
set feedback off

prompt <tasks>

prompt <task n="2.1">
prompt <![CDATA[

select * from PASSENGER;

prompt ]]>
prompt </task>


prompt <task n="2.2">
prompt <![CDATA[

select p_id, p_name, dateofbirth
from PASSENGER
where TIER = 'GOLD';

prompt ]]>
prompt </task>

prompt <task n="2.3">
prompt <![CDATA[

select P.P_ID, p_name, travel_date
from PASSENGER P
join BOOKING B on P.P_ID = B.P_ID
where DATEOFBIRTH > date '2010-01-01'
order by P_NAME desc, TRAVEL_DATE;

prompt ]]>
prompt </task>

prompt <task n="2.4">
prompt <![CDATA[

select p_name, travel_date
from PASSENGER P
join BOOKING B on P.P_ID = B.P_ID
where TIER is NULL
  and TRAVEL_DATE between date '2011-01-01' and date '2011-12-31'
order by P_NAME desc, TRAVEL_DATE;

prompt ]]>
prompt </task>

prompt <task n="2.5">
prompt <![CDATA[

select S.SEAT_ID, seat_row, seat_col, nvl2(TRAVEL_DATE, to_char(TRAVEL_DATE, 'YYYY/MM/DD'), 'NEM UTAZOTT') as date_of_travel
from SEAT S
full join BOOKING B on S.SEAT_ID = B.SEAT_ID
order by TRAVEL_DATE desc nulls first;

prompt ]]>
prompt </task>

prompt <task n="2.6">
prompt <![CDATA[

select P.P_ID, COUNT(BOOKING_ID) as foglalasok_szama
from PASSENGER P
full join BOOKING B on P.P_ID = B.P_ID
group by P.P_ID
order by foglalasok_szama;

prompt ]]>
prompt </task>

prompt <task n="2.7">
prompt <![CDATA[

select P.P_ID, COUNT(BOOKING_ID) as foglalasok_szama
from PASSENGER P
full join BOOKING B on P.P_ID = B.P_ID
group by P.P_ID
having COUNT(BOOKING_ID) < 3
order by P.P_ID;

prompt ]]>
prompt </task>

prompt <task n="2.8">
prompt <![CDATA[

select P.P_ID, P_NAME
from PASSENGER P
full join BOOKING B on P.P_ID = B.P_ID
group by P.P_ID, P_NAME
having count(BOOKING_DATE) = 0
order by P_NAME desc;

prompt ]]>
prompt </task>

prompt </tasks>